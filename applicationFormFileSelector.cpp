/*
    Copyright (C) 2013-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QLineEdit>
#include "pwmdFileDialog.h"
#include "applicationFormFileSelector.h"

ApplicationFormFileSelector::ApplicationFormFileSelector (QWidget *w,
							  QWidget *p)
  : QPushButton (p)
{
  _widget = w;
}

void
ApplicationFormFileSelector::slotFileSelector ()
{
  PwmdFileDialog d (this, tr ("Open file"));

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;

  QLineEdit *w = static_cast<QLineEdit *>(_widget);
  w->setText (d.selectedFiles ().at (0));
}
