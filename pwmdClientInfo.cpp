/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QStringList>
#include "pwmdClientInfo.h"

ClientInfo::ClientInfo (QString id, QString name, QString filename,
                        QString host, bool lock, bool self, unsigned state,
                        QString userId, QString username, time_t connected)
{
  _id = id;
  _name = name;
  _filename = filename.at(0) == '/' ? "-" : filename;
  _host = host;
  _lock = lock;
  _self = self;
  _state = state;
  _userId = userId;
  _username = username;
  _connectedAt = connected;
  _stale = 0;
}

ClientInfo::~ClientInfo ()
{
}

ClientInfo *
ClientInfo::parseLine (const QString &line)
{
  QStringList fields = line.split (" ");
  ClientInfo *ci = new ClientInfo (fields.at(0), fields.at(1), fields.at(2),
                                   fields.at(3), fields.at(4).toInt() == 1,
                                   fields.at(5).toInt() == 1,
                                   fields.at(6).toUInt(), fields.at(7),
                                   fields.at(8), fields.at(9).toUInt());
  return ci;
}

QString
ClientInfo::id ()
{
  return _id;
};

QString
ClientInfo::name ()
{
  return _name;
};

QString
ClientInfo::filename ()
{
  return _filename;
};

QString
ClientInfo::host ()
{
  return _host;
};

bool
ClientInfo::locked ()
{
  return _lock;
}

void
ClientInfo::setLocked (bool b)
{
  _lock = b;
}

bool
ClientInfo::self ()
{
  return _self;
}

unsigned
ClientInfo::state ()
{
  return _state;
}

void
ClientInfo::setState (unsigned s)
{
  _state = s;
}

QString
ClientInfo::userId ()
{
  return _userId;
}

QString
ClientInfo::username ()
{
  return _username;
}

time_t
ClientInfo::connectedAt ()
{
  return _connectedAt;
}

void
ClientInfo::setStale (time_t t)
{
  _stale = t;
}

time_t
ClientInfo::stale ()
{
  return _stale;
}
