VERSION="0.9.1"

# Android: where cross-compiled external libraries (libpwmd, etc) are located.
# The directoy structure should be:
#
#    DIR/arm64-v8a/{lib,include,bin}
#    DIR/armeabi-v7a/{lib,include,bin}
#    DIR/x86/{lib,include,bin}
#    DIR/x86_64/{lib,include,bin}
#
# depending on which arch you are building for. The required libraries are
# libpwmd, libassuan and libgpg-error. For ssh support, libssh2, libssl and
# libcrypto are also required. Also see README.android.
isEmpty (ANDROID_EXTRA_LIBS_ROOT) {
    ANDROID_EXTRA_LIBS_ROOT = $$(HOME)/projects/android/local
}

!isEmpty (PREFIX) {
}
else {
    PREFIX = /usr/local
}

BINDIR = $$PREFIX/bin
LIBDIR = $$PREFIX/lib
DATADIR = $$PREFIX/share/qpwmc

QMAKE_CXXFLAGS += $$(CXXFLAGS)
QMAKE_CFLAGS += $$(CFLAGS)
QMAKE_LFLAGS += $$(LDFLAGS)

TEMPLATE = app
QT += xml
QT += widgets
QT += core gui
QT += svg
CONFIG += release
#CONFIG += ppc ppc64

android: {
    for (abi, ANDROID_ABIS): {
	ANDROID_EXTRA_LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${abi}/lib/libassuan.so
	ANDROID_EXTRA_LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${abi}/lib/libgpg-error.so
	ANDROID_EXTRA_LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${abi}/lib/libpwmd.so
	LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${QT_ARCH}/lib/libassuan.so
	LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${QT_ARCH}/lib/libgpg-error.so
	LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${QT_ARCH}/lib/libpwmd.so

	contains (ANDROID_WITH_SSH, 1) {
	    ANDROID_EXTRA_LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${abi}/lib/libcrypto.so
	    ANDROID_EXTRA_LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${abi}/lib/libssl.so
	    ANDROID_EXTRA_LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${abi}/lib/libssh2.so
	    LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${QT_ARCH}/lib/libcrypto.so
	    LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${QT_ARCH}/lib/libssl.so
	    LIBS += $$ANDROID_EXTRA_LIBS_ROOT/$${QT_ARCH}/lib/libssh2.so
	}
    }

    QT += core-private
    #CONFIG += mobility console
    QMAKE_CFLAGS += $$(CFLAGS) -I$$ANDROID_EXTRA_LIBS_ROOT/include
    QMAKE_CXXFLAGS += $$(CXXFLAGS) -I$$ANDROID_EXTRA_LIBS_ROOT/$${QT_ARCH}/include
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

    HEADERS += $$PWD/pwmdPasswordPromptDialog.h \
               $$PWD/pwmdAndroidJNI.h
    SOURCES += $$PWD/pwmdPasswordPromptDialog.cpp \
	       $$PWD/pwmdAndroidJNI.cpp
    FORMS += $$PWD/pwmdPasswordPromptDialog.ui
}
else {
    CONFIG += link_pkgconfig
    PKGCONFIG += libpwmd-8.0
    CONFIG += x86 x86_64
    HEADERS += $$PWD/trayIcon.h \
               $$PWD/editShortcutDialog.h
    SOURCES += $$PWD/trayIcon.cpp \
               $$PWD/editShortcutDialog.cpp
    FORMS += $$PWD/editShortcutDialog.ui
}

win32: RC_ICONS = $$PWD/icons/win32.ico

RESOURCES += qpwmc.qrc
EXTRAS += trayicon.svg
EXTRAS += trayicon-linger.svg
EXTRAS += expiry.svg
EXTRAS += system-search.svg

contains (WITH_CLANG, 1) {
    include($$PWD/clang.conf)
}

contains (WITH_CLAZY, 1) {
    # You may need to export CLANGXX=clang++-X.Y in your environment.
    QMAKE_CXXFLAGS=-std=c++17
    QMAKE_CXX=clazy
}

HEADERS += $$PWD/pwmdMainWindow.h \
	   $$PWD/pwmdTreeWidget.h \
	   $$PWD/pwmdTreeWidgetItemData.h \
	   $$PWD/pwmdElementContent.h \
	   $$PWD/pwmdElementAttr.h \
	   $$PWD/pwmdListWidget.h \
	   $$PWD/pwmdGenPassDialog.h \
	   $$PWD/pwmdGenPassWidget.h \
	   $$PWD/pwmdPlainTextEdit.h \
	   $$PWD/pwmdToolButton.h \
	   $$PWD/pwmd.h \
	   $$PWD/pwmdRemoteHost.h \
	   $$PWD/pwmdSocketWidget.h \
	   $$PWD/pwmdSocketDialog.h \
	   $$PWD/applicationForm.h \
	   $$PWD/applicationFormXmlStream.h \
	   $$PWD/applicationFormWidget.h \
	   $$PWD/applicationFormRadioButton.h \
	   $$PWD/applicationFormPage.h \
	   $$PWD/applicationFormFinalizePage.h \
	   $$PWD/applicationFormFileSelector.h \
	   $$PWD/applicationFormGeneratePassword.h \
	   $$PWD/applicationFormExpiryButton.h \
	   $$PWD/applicationFormDateSelector.h \
	   $$PWD/pwmdKeyTreeWidget.h \
	   $$PWD/pwmdSaveWidget.h \
	   $$PWD/pwmdSaveDialog.h \
	   $$PWD/pwmdKey.h \
	   $$PWD/pwmdOptionsDialog.h \
	   $$PWD/pwmdClientDialog.h \
	   $$PWD/pwmdClientInfo.h \
	   $$PWD/pwmdOpenDialog.h \
	   $$PWD/pwmdFileOptionsDialog.h \
	   $$PWD/pwmdFileOptionsWidget.h \
	   $$PWD/cmdIds.h \
	   $$PWD/pwmdPasswordDialog.h \
	   $$PWD/pwmdExpireDialog.h \
	   $$PWD/pwmdKeyGenDialog.h \
	   $$PWD/pwmdLineEdit.h \
	   $$PWD/pwmdFileDialog.h \
	   $$PWD/pwmdRegExpValidator.h \
	   $$PWD/pwmdSearchDialog.h

SOURCES += $$PWD/main.cpp \
	   $$PWD/pwmdMainWindow.cpp \
	   $$PWD/pwmdGenPassDialog.cpp \
	   $$PWD/pwmdGenPassWidget.cpp \
	   $$PWD/pwmdTreeWidget.cpp \
	   $$PWD/pwmdTreeWidgetItemData.cpp \
	   $$PWD/pwmdElementContent.cpp \
	   $$PWD/pwmdElementAttr.cpp \
	   $$PWD/pwmdListWidget.cpp \
	   $$PWD/pwmdPlainTextEdit.cpp \
	   $$PWD/pwmdToolButton.cpp \
	   $$PWD/pwmd.cpp \
	   $$PWD/pwmdRemoteHost.cpp \
	   $$PWD/pwmdSocketWidget.cpp \
	   $$PWD/pwmdSocketDialog.cpp \
	   $$PWD/applicationForm.cpp \
	   $$PWD/applicationFormXmlStream.cpp \
	   $$PWD/applicationFormWidget.cpp \
	   $$PWD/applicationFormRadioButton.cpp \
	   $$PWD/applicationFormPage.cpp \
	   $$PWD/applicationFormFinalizePage.cpp \
	   $$PWD/applicationFormFileSelector.cpp \
	   $$PWD/applicationFormGeneratePassword.cpp \
	   $$PWD/applicationFormExpiryButton.cpp \
	   $$PWD/applicationFormDateSelector.cpp \
	   $$PWD/pwmdKeyTreeWidget.cpp \
	   $$PWD/pwmdSaveWidget.cpp \
	   $$PWD/pwmdSaveDialog.cpp \
	   $$PWD/pwmdKey.cpp \
	   $$PWD/pwmdOptionsDialog.cpp \
	   $$PWD/pwmdClientDialog.cpp \
	   $$PWD/pwmdClientInfo.cpp \
	   $$PWD/pwmdOpenDialog.cpp \
	   $$PWD/pwmdFileOptionsDialog.cpp \
	   $$PWD/pwmdFileOptionsWidget.cpp \
	   $$PWD/pwmdPasswordDialog.cpp \
	   $$PWD/pwmdExpireDialog.cpp \
	   $$PWD/pwmdKeyGenDialog.cpp \
	   $$PWD/pwmdLineEdit.cpp \
	   $$PWD/pwmdFileDialog.cpp \
	   $$PWD/pwmdRegExpValidator.cpp \
	   $$PWD/pwmdSearchDialog.cpp

FORMS += $$PWD/pwmdGenPassDialog.ui \
         $$PWD/pwmdGenPassWidget.ui \
         $$PWD/pwmdSocketDialog.ui \
         $$PWD/pwmdSocketWidget.ui \
	 $$PWD/applicationForm.ui \
	 $$PWD/applicationFormFinalizePage.ui \
	 $$PWD/pwmdSaveWidget.ui \
	 $$PWD/pwmdSaveDialog.ui \
	 $$PWD/pwmdMainWindow.ui \
	 $$PWD/pwmdOptionsDialog.ui \
	 $$PWD/pwmdClientDialog.ui \
	 $$PWD/pwmdOpenDialog.ui \
	 $$PWD/pwmdFileOptionsDialog.ui \
	 $$PWD/pwmdFileOptionsWidget.ui \
	 $$PWD/pwmdPasswordDialog.ui \
	 $$PWD/pwmdExpireDialog.ui \
	 $$PWD/pwmdKeyGenDialog.ui \
	 $$PWD/pwmdSearchDialog.ui

release.target = release
release.depends = $$TARGET
release.commands = $$PWD/makever.sh $$VERSION $$PWD release
QMAKE_EXTRA_TARGETS += release

deb.target = deb
deb.commands = dpkg-buildpackage -B -uc -rfakeroot
QMAKE_EXTRA_TARGETS += deb

TARGET = qpwmc
target.path = $$BINDIR

GITVERSION = main.h
gitversion.target = $$GITVERSION
gitversion.commands = $$PWD/makever.sh $$VERSION $$PWD
gitversion.depends = FORCE

QMAKE_EXTRA_TARGETS += gitversion

data.path += $$DATADIR
data.files += $$PWD/contrib/genericUserPassword.xml $$PWD/contrib/proxy.xml \
              $$PWD/contrib/mail.xml $$PWD/contrib/date.xml

manual.path += $$PREFIX/share/man/man1
manual.files += qpwmc.1

INSTALLS += target data manual

DISTFILES += \
    $$PWD/android/AndroidManifest.xml \
    $$PWD/android/gradle/wrapper/gradle-wrapper.jar \
    $$PWD/android/gradlew \
    $$PWD/android/res/values/libs.xml \
    $$PWD/android/build.gradle \
    $$PWD/android/gradle/wrapper/gradle-wrapper.properties \
    $$PWD/android/gradlew.bat
