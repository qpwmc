/*
    Copyright (C) 2012-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDREMOTEHOST_H
#define PWMDREMOTEHOST_H

#include <qglobal.h>
#include <QString>
#include <QMetaType>

class PwmdRemoteHost
{
 public:
  bool operator == (const PwmdRemoteHost &other) const;
  bool operator != (const PwmdRemoteHost &other) const;
  PwmdRemoteHost (QString name = QString ());
  ~PwmdRemoteHost ();

  QString name ();
  void setName (QString);
  int type ();
  void setType (int);		// pwmd_socket_t
  QString hostname ();
  void setHostname (QString);
  int port ();
  void setPort (int);
  int ipProtocol ();
  void setIpProtocol (int);
  QStringList socketArgs ();
  void setSocketArgs (QStringList);
  bool tlsVerify ();
  void setTlsVerify (bool);
  QString tlsPriority ();
  void setTlsPriority (QString);
  int socketTimeout ();
  void setSocketTimeout (int);
  int connectTimeout ();
  void setConnectTimeout (int);
  QString sshUsername ();
  void setSshUsername (QString);
  bool sshAgent ();
  void setSshAgent (bool);
#ifdef Q_OS_ANDROID
  QString clientCertificateAlias ();
  void setClientCertificateAlias (QString);
#endif
  /* Checks the config for a remote host 'name' and fills 'data' when
   * found. Returns false when no 'name' was found in the remote host
   * configuration. */
  static bool fillRemoteHost (const QString &name, PwmdRemoteHost &data);
  // Creates a URL from the PwmdRemoteHost members.
  static QString socketUrl (PwmdRemoteHost &);

 private:
  QString _name;
  int _type;			// pwmd_socket_t
  QString _hostname;
  int _port;
  int _ipProtocol;
  QStringList _socketArgs;
  bool _tlsVerify;
  QString _tlsPriority;
  int _socketTimeout;
  int _connectTimeout;
  QString _sshUsername;
  bool _sshAgent;
#ifdef Q_OS_ANDROID
  QString _clientCertificateAlias;
#endif
};

Q_DECLARE_METATYPE (PwmdRemoteHost);

#endif
