/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QApplication>
#include <QClipboard>
#include <QDateTime>
#include <QSettings>
#include <QRandomGenerator>
#include <sys/types.h>
#include <unistd.h>
#include "pwmdGenPassWidget.h"
#include <libpwmd.h>

PwmdGenPassWidget::PwmdGenPassWidget (QWidget * p) : QFrame (p)
{
  ui.setupUi (this);
  connect (ui.ck_showPassword, SIGNAL (clicked ()), this,
	   SLOT (slotShowPassword ()));
  connect (ui.pb_generate, SIGNAL (clicked ()), this,
	   SLOT (slotGeneratePassword ()));
  connect (ui.generateComboBox, SIGNAL (activated (int)), this,
	   SLOT (slotGenerateTypeChanged (int)));
  connect (ui.pb_clipboard, SIGNAL (clicked ()), this,
	   SLOT (slotClipboardPassword ()));

  connect (ui.constraintsComboBox, SIGNAL (activated (int)), this,
	   SLOT (slotConstraintsTypeChanged (int)));
  connect (ui.le_constraints, SIGNAL (textChanged (const QString &)), this,
           SLOT (slotConstraintsTextChanged (const QString &)));

  if (!(pwmd_features () & PWMD_FEATURE_QUALITY))
    ui.pg_strength->setHidden (true);
  else
    {
      connect (ui.le_password, SIGNAL (textChanged (const QString &)), this,
               SLOT (slotPasswordTextChanged (const QString &)));
      connect (ui.le_password, SIGNAL (textEdited (const QString &)), this,
               SLOT (slotPasswordTextChanged (const QString &)));
    }

  ui.le_constraints->setValidator (new QRegularExpressionValidator (QRegularExpression ("[^ ]*"), this));
  ui.pb_clipboard->setEnabled (false);
  _modified = false;
}

unsigned
PwmdGenPassWidget::quality (const QString &str, unsigned max, double &bits)
{
  if (!(pwmd_features () & PWMD_FEATURE_QUALITY))
    return 0;

  int r;
  bits = 0;
  (void)pwmd_test_quality (str.toUtf8 ().data (), &bits);

  r = (int)((bits/max)*100);
  if (r > 100)
    r = 100;

  return r;
}

void
PwmdGenPassWidget::slotConstraintsTextChanged (const QString &)
{
  setModified ();
}

void
PwmdGenPassWidget::slotPasswordTextChanged (const QString &str)
{
  double bits = 0;
  unsigned n = quality (str.toUtf8 ().data (), PwmdGenPassWidget::MaxEntropyBits, bits);

  ui.pg_strength->setValue (n);
  ui.pg_strength->setFormat (QString (tr ("Strength %p% (%1 bits)")).arg (bits));
  ui.pb_clipboard->setEnabled (!str.isEmpty ());
  ui.lengthSpinBox->setValue (str.isEmpty () ? 16 : str.length ());
  setModified ();
}

void
PwmdGenPassWidget::setPassword (const QString &s, bool ro)
{
  ui.le_password->setText (s);
  ui.le_password->setReadOnly (ro);
}

void
PwmdGenPassWidget::slotShowPassword ()
{
  bool b = ui.ck_showPassword->isChecked ();

  ui.le_password->setEchoMode (!b ? QLineEdit::Password : QLineEdit::Normal);
}

QString
PwmdGenPassWidget::constraints (const QString &s, bool &invert)
{
  QString constraint = ui.le_constraints->text ();
  invert = false;

  if (constraint.isEmpty ())
    return s;

  if (ui.constraintsComboBox->currentIndex () == 0)
    return s;
  else if (ui.constraintsComboBox->currentIndex () == 1)
    return s.isEmpty () ? constraint : s + "," + constraint;
  else
    {
      invert = true;
      return s.isEmpty () ? "! " + constraint : s + ",! " + constraint;
    }

  return QString ();
}

void
PwmdGenPassWidget::slotGeneratePassword ()
{
  QString result = QString ();
  bool invert = false;
  QString constraint = constraints (QString (), invert);

  switch (ui.generateComboBox->currentIndex ())
    {
    case 0:
      result = PwmdGenPassWidget::generate (PwmdGenPassWidget::Any,
                                            ui.lengthSpinBox->value (),
                                            constraint, invert);
      break;
    case 1:
      result = PwmdGenPassWidget::generate (PwmdGenPassWidget::AlphaNumeric,
                                            ui.lengthSpinBox->value (),
                                            constraint, invert);
      break;
    case 2:
      result = PwmdGenPassWidget::generate (PwmdGenPassWidget::Alpha,
                                            ui.lengthSpinBox->value (),
                                            constraint, invert);
      break;
    case 3:
      result = PwmdGenPassWidget::generate (PwmdGenPassWidget::Numeric,
                                            ui.lengthSpinBox->value (),
                                            constraint, invert);
      break;
    default:
      break;
    }

  ui.le_password->setText (result);
  slotClipboardPassword ();
}

void
PwmdGenPassWidget::slotConstraintsTypeChanged (int idx)
{
  ui.le_constraints->setEnabled (idx != 0);
  if (!idx)
    ui.le_constraints->setText ("");
  setModified ();
}

void
PwmdGenPassWidget::slotGenerateTypeChanged (int)
{
  setModified ();
}

QString
PwmdGenPassWidget::type ()
{
  bool invert;

  if (ui.generateComboBox->currentIndex () == 1)
    return constraints ("alnum", invert);
  else if (ui.generateComboBox->currentIndex () == 2)
    return constraints ("alpha", invert);
  else if (ui.generateComboBox->currentIndex () == 3)
    return constraints ("numeric", invert);

  return constraints (QString (), invert);
}

void
PwmdGenPassWidget::setType (const QString &s)
{
  QStringList t = s.split (',');
  QString type = t.count () ? t.at (0) : QString ();
  QString constraint = !t.isEmpty () && t.count () > 1 ? t.at (1) : QString ();

  if (type == "alnum")
    ui.generateComboBox->setCurrentIndex (1);
  else if (type == "alpha")
    ui.generateComboBox->setCurrentIndex (2);
  else if (type == "numeric")
    ui.generateComboBox->setCurrentIndex (3);
  else
    {
      if (t.count () == 1)
        constraint = s;
      ui.generateComboBox->setCurrentIndex (0);
    }

  ui.le_constraints->setEnabled (!constraint.isEmpty ());

  if (constraint.isEmpty ())
    ui.constraintsComboBox->setCurrentIndex (0);
  else if (constraint.at (0) != '!'
           || (constraint.at (0) == '!' && constraint.at (1) != ' '))
    ui.constraintsComboBox->setCurrentIndex (1);
  else
    {
      ui.constraintsComboBox->setCurrentIndex (2);
      constraint = constraint.mid (2);
    }

  ui.le_constraints->setText (constraint);
}

// Generate a passphrase of length 'len' from the characters in 'str'. The
// constraint adds characters after generating or removes in the case 'invert'
// is true.
QString
PwmdGenPassWidget::generate (const QString & str, int len, QString constraint,
                             bool invert)
{
  QString s = QString ();
  int max = 0;
  QSettings cfg ("qpwmc");
  int freq = cfg.value ("constraintsFreq", 30).toInt ();

  // Replace 30 percent of random characters in str of 'len' with the
  // constraint when not inverted.
  if (!constraint.isEmpty ())
    {
      max = len * freq / 100;
      if (!max)
        max++;
    }

  // Prevent inifinate loop when all characters of the constraint match some
  // order in 'str'.
  if (invert && !constraint.isEmpty ())
    {
      int n = 0;

      foreach (QChar c, constraint)
        {
          if (str.contains (c))
            n++;
        }

      if (n == str.length ())
        {
          constraint = QString ();
          invert = false;
        }
    }

  for (int i = 0; i < len; i++)
    {
      QChar c;

      // Don't include characters in 'constraint'.
      do
        {
          c = str.at (QRandomGenerator::system ()->generate () % str.length ());
        } while (invert && !constraint.isEmpty () && constraint.contains (c));

      s.append (c);
    }

  if (!invert && !constraint.isEmpty ())
    {
      QList <int> list;
      int count = 0;

      // Put 'max' random characters of the constraint in the result.
      do
        {
          int n;
          do
            {
              n = QRandomGenerator::system ()->generate () % s.length ();
            } while (list.contains (n) && list.length () < len);

          QChar c = constraint.at
            (QRandomGenerator::system ()->generate () % constraint.length ());
          s.replace (n, 1, c);
          list.append (n);
          if (list.length () == len - 1)
            break;
        }
      while (++count < max);
    }

  return s;
}

QString
PwmdGenPassWidget::generate (Type t, int len, const QString &constraint,
                             bool invert)
{
  QString result = QString ();
  static QRegularExpression re_n ("[0-9]+");

  switch (t)
    {
    case Any:
      result =
        generate (tr
                  ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`~-_=+!@#$%^&*()][\\{}|';\":?></.,"),
                  len, constraint, invert);
      break;
    case AlphaNumeric:
        {
          QRegularExpression re_a ("[a-z]+", QRegularExpression::CaseInsensitiveOption);

          do
            {
              result = generate (tr ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), len, constraint, invert);
            } while (len > 1 && !invert
                     && (constraint.isEmpty () || (!constraint.isEmpty () && len >= constraint.length ()))
                     && (!result.contains (re_n) || !result.contains (re_a)));
        }
      break;
    case Alpha:
      result =
        generate (tr
                  ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"),
                  len, constraint, invert);
      break;
    case Numeric:
      result = generate (tr ("0123456789"), len, constraint, invert);
      break;
    default:
      break;
    }

  return result;
}

QString
PwmdGenPassWidget::password ()
{
  return ui.le_password->text ();
}

void
PwmdGenPassWidget::clipboardPassword ()
{
  slotClipboardPassword ();
}

void
PwmdGenPassWidget::slotClipboardPassword ()
{
  QClipboard *c = QApplication::clipboard ();

  if (c->supportsSelection ())
    c->setText (ui.le_password->text (), QClipboard::Selection);
  c->setText (ui.le_password->text ());

  emit clipboardTimer ();
}

void
PwmdGenPassWidget::setModified (bool b)
{
  _modified = b;
  if (b)
    emit modified ();
}

bool
PwmdGenPassWidget::isModified ()
{
  return _modified;
}

void
PwmdGenPassWidget::setEditable (bool b)
{
  ui.generateLabel->setEnabled (b);
  ui.generateComboBox->setEnabled (b);
  ui.constraintsLabel->setEnabled (b);
  ui.constraintsComboBox->setEnabled (b);
  ui.le_constraints->setEnabled (b);
  ui.lengthSpinBox->setEnabled (b);
  ui.lengthLabel->setEnabled (b);
  ui.pb_generate->setEnabled (b);
  ui.pb_clipboard->setEnabled (true);
  ui.le_password->setReadOnly (!b);
}
