/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include "pwmdTreeWidgetItemData.h"

PwmdTreeWidgetItemData::PwmdTreeWidgetItemData (bool t, const QString & n,
						bool b)
{
  _name = n;
  _hasTarget = t;		// When parsing the output of LIST, not the attr
  _badTarget = b;
  _targetLoop = false;
  _badPermissions = false;
  _target = QString ();
  _attrs = new PwmdAttributeList ();
  _contentCached = false;
  _content = nullptr;
  _needsAttrRefresh = false;
}

PwmdTreeWidgetItemData::~PwmdTreeWidgetItemData ()
{
  clearAttributes ();
  _attrs->decrRefCount ();

  if (!_attrs->refCount ())
    delete _attrs;

  setContentCached (false);
}

PwmdElementContent *
PwmdTreeWidgetItemData::content ()
{
  return _content;
}

void
PwmdTreeWidgetItemData::setContent (PwmdElementContent * data)
{
  if (_content == data)
    return;

  if (!data)
    {
      setContentCached (false);
      return;
    }

  if (_content)
    setContentCached (false);

  _content = data;
  _content->incrRefCount ();
  setContentCached ();
}

bool
PwmdTreeWidgetItemData::contentCached ()
{
  return _contentCached;
}

void
PwmdTreeWidgetItemData::setContentCached (bool b)
{
  _contentCached = b;

  if (!b && _content)
    {
      _content->decrRefCount ();
      if (!_content->refCount ())
	delete _content;

      _content = nullptr;
    }
}

QString
PwmdTreeWidgetItemData::target ()
{
  return _target;
}

void
PwmdTreeWidgetItemData::setTarget (const QString & s)
{
  _target = s;
}

bool
PwmdTreeWidgetItemData::hasTarget ()
{
  return _hasTarget;
}

bool
PwmdTreeWidgetItemData::badTarget ()
{
  return _badTarget;
}

void
PwmdTreeWidgetItemData::setBadTarget (bool b)
{
  _badTarget = b;
}

bool
PwmdTreeWidgetItemData::targetLoop ()
{
  return _targetLoop;
}

void
PwmdTreeWidgetItemData::setTargetLoop (bool b)
{
  _targetLoop = b;
}

QString
PwmdTreeWidgetItemData::name ()
{
  return _name;
}

QString
PwmdTreeWidgetItemData::attribute (const QString & name)
{
  for (int i = 0, t = _attrs->count (); i < t; i++)
    {
      PwmdElementAttr *attr = _attrs->at (i);

      if (attr->name () == name)
	return attr->value ();
    }

  return QString ();
}

bool
PwmdTreeWidgetItemData::hasAttribute (const QString & name, bool remove)
{
  for (int i = 0, t = _attrs->count (); i < t; i++)
    {
      PwmdElementAttr *attr = _attrs->at (i);

      if (attr->name () == name)
	{
	  if (remove)
	    {
	      _attrs->removeAt (i);
	      delete attr;
	    }
	  return true;
	}
    }

  return false;
}

PwmdAttributeList *
PwmdTreeWidgetItemData::attributes ()
{
  return _attrs;
}

bool
PwmdTreeWidgetItemData::removeAttribute (const QString & name)
{
  return hasAttribute (name, true);
}

void
PwmdTreeWidgetItemData::clearAttributes ()
{
  while (!_attrs->isEmpty ())
    delete _attrs->takeFirst ();
}

static bool
sortAttributesCb (PwmdElementAttr *a, PwmdElementAttr *b)
{
  return a->name ().toLower () < b->name ().toLower ();
}

static void
sortAttributes (PwmdAttributeList *list)
{
  std::sort (list->begin (), list->end (), sortAttributesCb);
  int i = 0;

  for (i = list->count ()-1; i >= 0; i--)
    {
      PwmdElementAttr *a = list->at (i);
      if (PwmdElementAttr::isReservedAttribute (a->name ()))
        break;
    }

  for (int n = 0; i && n < i; n++)
    {
      PwmdElementAttr *a = list->at (n);
      if (PwmdElementAttr::isReservedAttribute (a->name ()))
        continue;

      list->move (n--, i--);
    }

  for (i = 0; i < list->count (); i++)
    {
      PwmdElementAttr *a = list->at (i);
      if (!PwmdElementAttr::isReservedAttribute (a->name ()))
        break;
    }

again:
  for (int n = i; n < list->count (); n++)
    {
      PwmdElementAttr *a = list->at (n);

      for (int x = n+1; x < list->count (); x++)
        {
          int r = a->name ().localeAwareCompare (list->at (x)->name ());

          if (r > 0)
            {
              list->move (n, x);
              goto again;
            }
        }
    }
}

void
PwmdTreeWidgetItemData::addAttribute (PwmdAttributeList *list,
				      const QString &name,
				      const QString &value)
{
  for (int i = 0, t = list->count (); i < t; i++)
    {
      PwmdElementAttr *attr = list->at (i);

      if (attr->name () == name)
	{
	  attr->setValue (value);
	  return;
	}
    }

  list->append (new PwmdElementAttr (name, value));
  sortAttributes (list);
}

void
PwmdTreeWidgetItemData::addAttribute (const QString & name,
				      const QString & value)
{
  addAttribute (_attrs, name, value);
}

void
PwmdTreeWidgetItemData::setAttributes (PwmdAttributeList * attrs)
{
  if (_attrs == attrs)
    return;

  clearAttributes ();
  _attrs->decrRefCount ();
  if (!_attrs->refCount ())
    delete _attrs;

  _attrs = attrs ? attrs : new PwmdAttributeList ();
  _attrs->incrRefCount ();
  sortAttributes (_attrs);
}

bool
PwmdTreeWidgetItemData::needsAttrRefresh()
{
  return _needsAttrRefresh;
}

void
PwmdTreeWidgetItemData::setNeedsAttrRefresh(bool b)
{
  _needsAttrRefresh = b;
}

bool
PwmdTreeWidgetItemData::badPermissions ()
{
  return _badPermissions;
}

void
PwmdTreeWidgetItemData::setBadPermissions (bool b)
{
  _badPermissions = b;
}

bool
PwmdTreeWidgetItemData::hasError ()
{
  return badPermissions () || targetLoop () || badTarget ();
}
