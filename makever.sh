#!/bin/sh
VERSION="$1"
if [ $# -eq 2 -a -f "$2/.git/HEAD" ]; then
    BRANCH="`cut -d / -f 3 $2/.git/HEAD`"
    VERSION="$VERSION-`cut -b -7 $2/.git/refs/heads/$BRANCH`"
fi

cat << EOF > "$2/main.h.in"
#ifndef MAIN_H
#define MAIN_H

#define QPWMC_VERSION "$VERSION"

#endif
EOF

cmp --quiet "$2/main.h.in" "$2/main.h"
if [ $? != 0 ]; then
    mv -f "$2/main.h.in" "$2/main.h"
else
    rm -f "$2/main.h.in"
fi

if [ $# -ne 3 ]; then
    exit 0
fi

set -e
ROOT="qpwmc-`sed -ne '/VERSION/{s/"//g;p;q}' < main.h | \
    awk '{print $3}'`"
TGZ="$ROOT.tar.bz2"

echo "Creating $ROOT"
mkdir -p $ROOT

echo "Updating ChangeLog ..."
git log > $ROOT/ChangeLog

echo "Copying git controlled files ..."
for f in `git ls-files`; do
    mkdir -p "$ROOT/`dirname $f`"
    echo "Copying $f"
    cp -R $f "$ROOT/$f"
done

tar jcf "$TGZ" "$ROOT"
