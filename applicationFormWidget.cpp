/*
   Copyright (C) 2013-2025 Ben Kibbey <bjk@luxsci.net>

   This file is part of qpwmc.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
   USA
   */
#include <QSpinBox>
#include <QLineEdit>
#include <QCheckBox>

#include "applicationFormWidget.h"
#include "applicationFormDateSelector.h"

ApplicationFormWidget::ApplicationFormWidget ()
{
  _elementSelector = false;
  _dateSelector = false;
  _id = QString ();
  _widget = nullptr;
  _type = AppWidgetNone;
  _childOf = QString ();
  _value = QString ();
  _pwmdName = QString ();
  _label = QString ();
  _isHidden = false;
  _isRequired = false;
  _whatsThis = QString ();
  _fileSelector = false;
  _passwordGenerator = false;
  _expiry = false;
  _expiryDate = 0;
  _expiryAge = 0;
  _placeholderText = QString ();
}

ApplicationFormWidget::~ApplicationFormWidget ()
{
  foreach (ApplicationFormAttr *a, _attrs)
    delete a;

  if (_widget)
    delete _widget;
}

bool
ApplicationFormWidget::expiry ()
{
  return _expiry;
}

time_t
ApplicationFormWidget::expiryDate ()
{
  return _expiryDate;
}

time_t
ApplicationFormWidget::expiryAge ()
{
  return _expiryAge;
}

void
ApplicationFormWidget::setExpiry (time_t e, time_t age)
{
  _expiry = true;

  if (e)
    {
      setAttr ("_expire", QString::number (e));
      setAttr ("_age", QString::number (age));
    }

  _expiryDate = e;
  _expiryAge = age;
}

void
ApplicationFormWidget::setType (int n)
{
  if (_type != n && _widget)
    {
      delete _widget;
      _widget = nullptr;
    }

  _type = n;
  switch (_type)
    {
    case AppWidgetInteralOverwriteCheckBox:
    case AppWidgetCheckBox:
      _widget = new QCheckBox ();
      break;
    case AppWidgetRadioButton:
      break;
    case AppWidgetSpinBox:
        {
          QSpinBox *s = new QSpinBox ();
          s->setMaximum (999999);
          _widget = s;
        }
      break;
    case AppWidgetDateSelector:
        {
          ApplicationFormDateSelector *w = new ApplicationFormDateSelector (_value);
          w->setClearButtonEnabled (true);
          _widget = w;
        }
      break;
    case AppWidgetLineEdit:
        {
          QLineEdit *w = new QLineEdit (_value);
          if (isHidden ())
            {
              w->setEchoMode (QLineEdit::Password);
              w->setInputMethodHints (Qt::ImhNoPredictiveText|Qt::ImhNoAutoUppercase|Qt::ImhSensitiveData);
            }
          w->setClearButtonEnabled (true);
          _widget = w;
        }
      break;
    default:
      break;
    }
}

int
ApplicationFormWidget::type ()
{
  return _type;
}

QWidget *
ApplicationFormWidget::widget ()
{
  return _widget;
}

void
ApplicationFormWidget::setValue (QString s)
{
  _value = s;

  if (type () == AppWidgetNone)
    return;

  switch (type ())
    {
    case AppWidgetInteralOverwriteCheckBox:
    case AppWidgetCheckBox:
        {
          QCheckBox *w = static_cast<QCheckBox *>(_widget);
          w->setChecked (s.toInt ());
        }
      break;
    case AppWidgetSpinBox:
        {
          QSpinBox *w = static_cast<QSpinBox *>(_widget);
          w->setValue (s.toInt ());
        }
      break;
    case AppWidgetRadioButton:
      break;
    case AppWidgetDateSelector:
        {
          ApplicationFormDateSelector *w = static_cast<ApplicationFormDateSelector*> (_widget);
          w->setText (s);
        }
      break;
    case AppWidgetLineEdit:
        {
          QLineEdit *w = static_cast<QLineEdit *>(_widget);
          w->setText (s);
          if (isHidden ())
            w->setEchoMode (QLineEdit::Password);
        }
      break;
    default:
      break;
    }
}

QString
ApplicationFormWidget::value ()
{
  switch (type ())
    {
    case AppWidgetInteralOverwriteCheckBox:
    case AppWidgetCheckBox:
        {
          QCheckBox *w = static_cast<QCheckBox *>(_widget);
          return QString ("%1").arg (w->isChecked ());
        }
      break;
    case AppWidgetSpinBox:
        {
          QSpinBox *w = static_cast<QSpinBox *>(_widget);
          return QString ("%1").arg (w->value ());
        }
      break;
    case AppWidgetRadioButton:
      break;
    case AppWidgetDateSelector:
        {
          ApplicationFormDateSelector *w = static_cast<ApplicationFormDateSelector *> (_widget);
          return w->text ();
        }
      break;
    case AppWidgetLineEdit:
        {
          QLineEdit *w = static_cast<QLineEdit *>(_widget);
          return w->text ();
        }
      break;
    default:
      break;
    }

  return _value;
}

void
ApplicationFormWidget::setElementSelector (bool b)
{
  _elementSelector = b;
}

bool
ApplicationFormWidget::elementSelector ()
{
  return _elementSelector;
}
void
ApplicationFormWidget::setFileSelector (bool b)
{
  _fileSelector = b;
}

bool
ApplicationFormWidget::fileSelector ()
{
  return _fileSelector;
}

void
ApplicationFormWidget::setDateSelector (bool b)
{
  _dateSelector = b;
}

bool
ApplicationFormWidget::dateSelector ()
{
  return _dateSelector;
}

void
ApplicationFormWidget::setId (QString id)
{
  _id = id;
}

QString
ApplicationFormWidget::id ()
{
  return _id;
}

void
ApplicationFormWidget::setChildOf (QString s)
{
  _childOf = s;
}

QString
ApplicationFormWidget::childOf ()
{
  return _childOf;
}

void
ApplicationFormWidget::setPwmdName (QString s)
{
  _pwmdName = s;
}

QString
ApplicationFormWidget::pwmdName ()
{
  return _pwmdName;
}

void
ApplicationFormWidget::setPlaceholderText (const QString &s)
{
  if (type () == ApplicationFormWidget::AppWidgetLineEdit
      || type () == ApplicationFormWidget::AppWidgetDateSelector)
    {
      QLineEdit *w = static_cast<QLineEdit *>(widget ());
      w->setPlaceholderText (s);
    }

  _placeholderText = s;
}

QString
ApplicationFormWidget::placeholderText ()
{
  return _placeholderText;
}

void
ApplicationFormWidget::setLabel ( QString s)
{
  _label = s;
}

QString
ApplicationFormWidget::label ()
{
  return _label;
}

void
ApplicationFormWidget::addRadioButton (QString s)
{
  _radioButtons.append (s);
}

QStringList
ApplicationFormWidget::radioButtons ()
{
  return _radioButtons;
}

void
ApplicationFormWidget::addRadioButtonId (int n)
{
  _radioButtonIds.append (n);
}

bool
ApplicationFormWidget::hasRadioButtonId (int n)
{
  for (int i = 0; i < _radioButtonIds.count (); i++)
    {
      if (_radioButtonIds.at (i) == n)
        return true;
    }

  return false;
}

void
ApplicationFormWidget::setHidden ()
{
  if (_widget)
    {
      QLineEdit *w = static_cast<QLineEdit *>(_widget);
      w->setEchoMode (QLineEdit::Password);
    }

  _isHidden = true;
  setAttr ("hidden");
  setAttr ("password");
}

bool
ApplicationFormWidget::isHidden ()
{
  return _isHidden;
}

void
ApplicationFormWidget::setRequired ()
{
  _isRequired = true;
}

bool
ApplicationFormWidget::isRequired ()
{
  return _isRequired;
}

void
ApplicationFormWidget::setWhatsThis (QString s)
{
  _whatsThis = s;
}

QString
ApplicationFormWidget::whatsThis ()
{
  return _whatsThis;
}

void
ApplicationFormWidget::setPasswordGenerator (bool b)
{
  _passwordGenerator = b;
}

bool
ApplicationFormWidget::passwordGenerator ()
{
  return _passwordGenerator;
}

QString
ApplicationFormWidget::attr (const QString &name)
{
  foreach (ApplicationFormAttr *a, attrs())
    {
      if (a->name () == name)
        return a->value ();
    }

  return QString ();
}

void
ApplicationFormWidget::setAttr (QString name, QString value)
{
  int i;

  for (i = 0; i < _attrs.count (); i++)
    {
      ApplicationFormAttr *a = _attrs.at (i);

      if (a->name () == name)
        {
          _attrs.removeAt (i);
          delete a;
          break;
        }
    }

  _attrs.append (new ApplicationFormAttr (name, value));
}

QList<ApplicationFormAttr *>
ApplicationFormWidget::attrs ()
{
  return _attrs;
}
