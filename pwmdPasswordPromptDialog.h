/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDPASSWORDPROMPTDIALOG_H
#define PWMDPASSWORDPROMPTDIALOG_H

#include <QDialog>
#include "ui_pwmdPasswordPromptDialog.h"

class PwmdPasswordPromptDialog : public QDialog
{
 Q_OBJECT
 public:
   PwmdPasswordPromptDialog (QWidget * = 0);
   ~PwmdPasswordPromptDialog ();
   QString passphrase ();
   void setDescription (const QString &);
   void setPrompt (const QString &);
   void setRepeat (bool = true);
   void setQuality (bool);

 private slots:
   void slotPassphraseChanged (const QString &);

 private:
   Ui::PwmdPasswordPromptDialog ui;
};

#endif
