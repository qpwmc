/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDTREEWIDGETITEMDATA_H
#define PWMDTREEWIDGETITEMDATA_H

#include <QMetaType>
#include "pwmdElementContent.h"
#include "pwmdElementAttr.h"

class PwmdTreeWidgetItemData
{
 public:
  PwmdTreeWidgetItemData () { };
  PwmdTreeWidgetItemData (bool t, const QString & n, bool b = false);
  PwmdTreeWidgetItemData (const PwmdTreeWidgetItemData & other);
  ~PwmdTreeWidgetItemData ();

  QString target ();
  void setTarget (const QString & s);
  bool hasTarget ();
  bool badTarget ();
  void setBadTarget (bool b = true);
  bool targetLoop ();
  void setTargetLoop (bool b = true);
  bool badPermissions ();
  void setBadPermissions (bool b = true);
  bool hasError ();
  QString name ();
  QString attribute (const QString & name);
  bool hasAttribute (const QString & name, bool remove = false);
  PwmdAttributeList *attributes ();
  bool removeAttribute (const QString & name);
  void clearAttributes ();
  void addAttribute (const QString & name, const QString & value = 0);
  void addAttribute (PwmdAttributeList *, const QString & name,
		     const QString & value = 0);
  void setAttributes (PwmdAttributeList *);
  PwmdElementContent *content ();
  void setContent (PwmdElementContent *);
  bool contentCached ();
  void setContentCached (bool = true);
  bool needsAttrRefresh();
  void setNeedsAttrRefresh(bool = true);

 private:
  bool _hasTarget;
  bool _badTarget;
  bool _targetLoop;
  bool _badPermissions;
  QString _name;
  QString _target;
  PwmdAttributeList *_attrs;
  PwmdElementContent *_content;
  bool _contentCached;
  bool _needsAttrRefresh;
};

Q_DECLARE_METATYPE (PwmdTreeWidgetItemData *);

#endif
