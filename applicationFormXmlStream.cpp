/*
    Copyright (C) 2013-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QApplication>
#include <QMessageBox>
#include <QFile>
#include "applicationFormXmlStream.h"

ApplicationFormXmlStream::ApplicationFormXmlStream (const QString &filename,
                                                    ApplicationForm *w)
{
  inWidget = 0;
  currentAppString = AppStringNone;
  wizard = w;
  currentElement = ElementNone;
  currentWidget = nullptr;

  QFile file (filename);
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  stream = new QXmlStreamReader (&file);

  do {
      QXmlStreamReader::TokenType type = stream->readNext ();
      QString name = stream->name ().toUtf8().data ();

      switch (type)
        {
        case QXmlStreamReader::Invalid:
        case QXmlStreamReader::StartDocument:
        case QXmlStreamReader::EndDocument:
        case QXmlStreamReader::Comment:
        case QXmlStreamReader::DTD:
        case QXmlStreamReader::EntityReference:
        case QXmlStreamReader::ProcessingInstruction:
        case QXmlStreamReader::NoToken:
          break;
        case QXmlStreamReader::StartElement:
          if (name == "formRow")
            {
              if (inWidget)
                break;

              inWidget = true;
              currentAppString = AppStringNone;
              currentWidget = new ApplicationFormWidget ();
              break;
            }
          else if (!inWidget)
            {
              if (name == "PwmdApplicationForm")
                break;
              else if (name == "name")
                currentAppString = AppStringName;
              else if (name == "shortDescription")
                currentAppString = AppStringShortDescription;
              else if (name == "description")
                currentAppString = AppStringDescription;

              break;
            }

          if (name == "type")
            {
              currentElement = ElementType;
            }
          else if (name == "elementSelector")
            {
              currentWidget->setElementSelector ();
              currentElement = ElementSelector;
            }
          else if (name == "fileSelector")
            {
              currentWidget->setFileSelector ();
              currentElement = ElementFileSelector;
            }
          else if (name == "dateSelector")
            {
              currentWidget->setDateSelector ();
              currentElement = ElementDateSelector;
            }
          else if (name == "passwordGenerator")
            {
              currentWidget->setPasswordGenerator ();
              currentElement = ElementPasswordGenerator;
            }
          else if (name == "id")
            {
              currentElement = ElementId;
            }
          else if (name == "childOf")
            {
              currentElement = ElementChildOf;
            }
          else if (name == "value")
            {
              currentElement = ElementValue;
            }
          else if (name == "element")
            {
              currentElement = ElementPwmdName;
            }
          else if (name == "label")
            {
              currentElement = ElementLabel;
            }
          else if (name == "placeholderText")
            {
              currentElement = ElementPlaceholderText;
            }
          else if (name == "radio")
            {
              currentElement = ElementRadio;
            }
          else if (name == "hiddenInput")
            {
              currentWidget->setHidden ();
              currentElement = ElementHiddenInput;
            }
          else if (name == "expiry")
            {
              currentWidget->setExpiry ();
              currentElement = ElementExpiry;
            }
          else if (name == "required")
            {
              currentWidget->setRequired ();
              currentElement = ElementRequired;
            }
          else if (name == "whatsThis")
            {
              currentElement = ElementWhatsThis;
            }
          break;
        case QXmlStreamReader::EndElement:
          if (name == "formRow")
            {
              inWidget = false;
              if (wizard->pwm && (currentWidget->id () == "pwmdSocket"
                                  || currentWidget->id () == "pwmdFilename"))
                delete currentWidget;
              else
                _widgets.append (currentWidget);
              currentWidget = nullptr;
            }
          break;
        case QXmlStreamReader::Characters:
          QString chars = stream->text ().toUtf8 ().data ();
          if (chars.trimmed ().isEmpty ())
            break;

          switch (currentAppString)
            {
            case AppStringName:
              wizard->ui.wz_start->setTitle (chars);
              break;
            case AppStringShortDescription:
              wizard->ui.wz_start->setSubTitle (chars);
              break;
            case AppStringDescription:
              wizard->ui.wz_startTextEdit->document ()->setHtml (chars);
              break;
            default:
              break;
            }

          switch (currentElement)
            {
            case ElementType:
              if (chars == "radioButton")
                {
                  currentWidget->setType (ApplicationFormWidget::AppWidgetRadioButton);
                }
              else if (chars == "spinBox")
                {
                  currentWidget->setType (ApplicationFormWidget::AppWidgetSpinBox);
                }
              else if (chars == "lineEdit")
                {
                  if (currentWidget->dateSelector ())
                    currentWidget->setType (ApplicationFormWidget::AppWidgetDateSelector);
                  else
                    currentWidget->setType (ApplicationFormWidget::AppWidgetLineEdit);
                }
              else if (chars == "checkBox")
                {
                  currentWidget->setType (ApplicationFormWidget::AppWidgetCheckBox);
                }
              break;
            case ElementId:
              currentWidget->setId (QString (chars));
              break;
            case ElementChildOf:
              currentWidget->setChildOf (chars);
              break;
            case ElementValue:
              currentWidget->setValue (chars);
              break;
            case ElementPwmdName:
              currentWidget->setPwmdName (chars);
              break;
            case ElementLabel:
              currentWidget->setLabel (chars);
              break;
            case ElementPlaceholderText:
              currentWidget->setPlaceholderText (chars);
              break;
            case ElementRadio:
              currentWidget->addRadioButton (chars);
              break;
            case ElementHiddenInput:
              currentWidget->setHidden ();
              break;
            case ElementExpiry:
              currentWidget->setExpiry ();
              break;
            case ElementRequired:
              currentWidget->setRequired ();
              break;
            case ElementWhatsThis:
              currentWidget->setWhatsThis (chars);
              break;
            default:
              break;
            }
          break;
        }
  } while (!stream->atEnd ());

  currentWidget = new ApplicationFormWidget ();
  currentWidget->setType (ApplicationFormWidget::AppWidgetInteralOverwriteCheckBox);
  currentWidget->setWhatsThis (QApplication::tr ("Allow overwriting of existing element content."));
  currentWidget->setLabel (QApplication::tr ("Overwrite content:"));
  _widgets.append (currentWidget);
  currentWidget = nullptr;

  if (stream->error () != QXmlStreamReader::NoError)
    {
      QMessageBox m;
      m.setText (file.fileName ());
      m.setInformativeText (QString ("%1\n\nLine %2, position %3.").arg (stream->errorString ()).arg (stream->lineNumber ()).arg (stream->columnNumber ()));
      m.setIcon (QMessageBox::Critical);
      m.exec ();
      return;
    }
}

ApplicationFormXmlStream::~ApplicationFormXmlStream ()
{
  while (_widgets.count ())
    {
      ApplicationFormWidget *w = _widgets.takeFirst ();
      delete w;
    }

  _widgets.clear ();
  delete stream;
}

QList<ApplicationFormWidget *>
ApplicationFormXmlStream::widgets ()
{
  return _widgets;
}

QXmlStreamReader::Error
ApplicationFormXmlStream::error ()
{
  return stream->error ();
}
