/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDTREEWIDGET_H
#define PWMDTREEWIDGET_H

#include <QApplication>
#include <QTreeWidget>
#include <QTimer>
#include <QLineEdit>
#include <QItemDelegate>
#include <QRegularExpression>
#include <QKeyEvent>
#include <QResizeEvent>
#include "pwmdTreeWidgetItemData.h"
#include "pwmdRegExpValidator.h"

class PwmdMainWindow;
class Pwmd;
class PwmdTreeWidget : public QTreeWidget
{
 Q_OBJECT
 public:
  PwmdTreeWidget (QWidget * = 0);
  ~PwmdTreeWidget () { };
  void setPwmdParent (PwmdMainWindow *);
  void setPwmdHandle (Pwmd *);
  QPoint mousePosition ();

 signals:
  void treeWidgetResized (const QSize &);

 private slots:
  void slotExpandTimeout ();
  void slotAttributesRetrieved (QTreeWidgetItem *);
  void slotBusy (int, bool);

 private:
  void resizeEvent (QResizeEvent *);
  void keyPressEvent (QKeyEvent *);
  void mousePressEvent (QMouseEvent *);
  void mouseReleaseEvent (QMouseEvent *);
  void mouseMoveEvent (QMouseEvent *);
  void dragEnterEvent (QDragEnterEvent *);
  void dragMoveEvent (QDragMoveEvent *);
  void popupDragContextMenu (QTreeWidgetItem * = 0);
  QStringList mimeTypes () const;
  Qt::DropActions supportedDropActions ()const;
  bool dropMimeData (QTreeWidgetItem *, int, const QMimeData *,
		     Qt::DropAction);
  bool isValidDndMove (QTreeWidgetItem *, bool = false);
  bool dropIsChild (QTreeWidgetItem *src, QTreeWidgetItem *drop);
  bool validTargetDropElement (QTreeWidgetItem *);
  void setPopupMenuItemsEnabled ();

  PwmdMainWindow *_parent;
  QPoint dragStartPosition;
  QTimer *expandTimer;
  QTreeWidgetItem *lastItem;
  bool dndMove;
  QPoint _mousePosition;
};

class PwmdTreeWidgetItemDelegate:public QItemDelegate
{
 Q_OBJECT
 public:
  PwmdTreeWidgetItemDelegate (QObject *p = 0):QItemDelegate (p) { };
  QWidget *createEditor (QWidget *p, const QStyleOptionViewItem &o,
			 const QModelIndex &i) const
  {
    QWidget *editor = QItemDelegate::createEditor (p, o, i);
    QLineEdit *lineEdit = qobject_cast < QLineEdit * >(editor);

    if (lineEdit)
      {
        // There is a mismatch bug (I think) of signal/slot for
        // cursorPositionChanged() on Android and the new element cannot be
        // created when the validator is enabled. Pwmd will return a syntax
        // error when needed so disable it on Android, for now.
#ifndef Q_OS_ANDROID
        QRegularExpression rx ("^[\\S]+$");
        PwmdRegExpValidator *v = new PwmdRegExpValidator (rx, lineEdit);
        lineEdit->setValidator (v);
#endif
        lineEdit->setInputMethodHints (Qt::ImhNoPredictiveText|Qt::ImhNoAutoUppercase);
      }
#ifdef Q_OS_ANDROID
    qApp->inputMethod ()->show ();
#endif

    return editor;
  };
};

#endif
