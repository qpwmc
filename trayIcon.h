/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef TRAYICON_H
#define TRAYICON_H

#include <QSystemTrayIcon>
#include <QDateTime>
#include <QMenu>
#include "editShortcutDialog.h"
#include "pwmd.h"
#include "pwmdRemoteHost.h"

class TrayIcon:public QSystemTrayIcon
{
 Q_OBJECT
 public:
  TrayIcon ();
  ~TrayIcon ();

 signals:
  void knownHostRc (gpg_error_t);

 private slots:
  void slotSystemTrayActivated (QSystemTrayIcon::
				ActivationReason);
  void slotConnectionStateChanged (Pwmd::ConnectionState);
  void slotEditShortcuts ();
  void slotShortCut (QAction *);
  void slotClearClipboard ();
  void slotClearClipboardReal ();
  void slotAbout ();
  void slotEditPwmd ();
  void slotQuit ();
  void slotCommandResult (PwmdCommandQueueItem *item, QString result,
                          gpg_error_t rc, bool queued);
  void slotKnownHostCallback (void *data, const char *host, const char *key,
                              size_t len);
  void slotBusy (int, bool);
  void slotRotateIcon ();
  void slotStatusMessage (QString line, void *);
  void slotLinger ();

 private:
  void refreshShortcuts ();
  void shortCutFinalize (const QString &);
  void showMessage (gpg_error_t, bool reset = true);
  void rotateIcon (bool reset = false);
  void startStopTimer (bool start);

  int clipboardTimeout;
  QMenu *shortcutMenu;
  QTimer *clipboardTimer;
  PwmdRemoteHost currentHostData;
  Pwmd *pwm;
  QTimer *rotateTimer;
  QTimer *lingerTimer;
  int lingerTime;
  int lingerRemaining;
  bool closeFile;
  QString *statusData;
};

#endif
