/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PWMDOPENDIALOG_H
#define PWMDOPENDIALOG_H

#include <QDialog>
#include "pwmd.h"
#include "ui_pwmdOpenDialog.h"

class FilenameData
{
 public:
  FilenameData (QString f, QString a, QString m, QString c)
    {
      _name = f;
      _access = a;
      _mod = m;
      _change = c;
    };

  ~FilenameData () {};
  QString name ()
    {
      return _name;
    };
  QString access ()
    {
      return _access;
    };
  QString mod ()
    {
      return _mod;
    };
  QString change ()
    {
      return _change;
    };

 private:
  QString _name;
  QString _access;
  QString _mod;
  QString _change;
};

class PwmdOpenDialog : public QDialog
{
 Q_OBJECT
 public:
  PwmdOpenDialog (Pwmd *, QWidget *parent = 0);
  ~PwmdOpenDialog ();
  QString filename ();
  QString keyFile ();
  bool lock ();

 private slots:
  void slotNewFilename ();
  void slotRefreshFilenameList ();
  void slotFilenameSelected (QListWidgetItem *, QListWidgetItem *);
  void slotOpenEditorClosed (QWidget *, QAbstractItemDelegate::EndEditHint);
  void slotConfirmNewFile (QWidget *);
  void slotFilenameDoubleClicked (QListWidgetItem *);
  void slotCommandResult (PwmdCommandQueueItem *item, QString result,
                          gpg_error_t rc, bool queued);

 private:
  bool event (QEvent *);
  void refreshFilenameListFinalize (const QString &);
  void fileStatFinalize (const QString &);

  Ui::PwmdOpenDialog ui;
  QListWidgetItem *newFilename;
  Pwmd *pwm;
};

Q_DECLARE_METATYPE (FilenameData *);

#endif
