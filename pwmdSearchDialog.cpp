/*
    Copyright (C) 2021-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QComboBox>
#include <QLineEdit>
#include <QSettings>
#include <QCommonStyle>
#include <QAction>
#include "pwmdLineEdit.h"
#include "pwmdSearchDialog.h"

PwmdSearchDialog::PwmdSearchDialog (QWidget *p) : QDialog (p)
{
  ui.setupUi (this);
  connect (ui.pb_find, SIGNAL (clicked ()), this,
           SLOT (slotFindNext ()));
  ui.pb_find->setIcon (QIcon (":icons/system-search.svg"));
  connect (ui.ck_attr, SIGNAL (stateChanged (int)), this,
           SLOT (slotAttrChanged (int)));
  connect (ui.ck_attrValue, SIGNAL (stateChanged (int)), this,
           SLOT (slotAttrValueChanged (int)));
  connect (ui.ck_elements, SIGNAL (stateChanged (int)), this,
           SLOT (slotElementChanged (int)));
  connect (ui.ck_select, SIGNAL (stateChanged (int)), this,
           SLOT (slotSelectElementsChanged (int)));
  PwmdLineEdit *le = new PwmdLineEdit;
  ui.cb_find->setLineEdit (le);
  le->setClearButtonEnabled (true);
  ui.cb_find->setInsertPolicy (QComboBox::InsertPolicy::InsertAtTop);
  connect (ui.cb_find->lineEdit (), SIGNAL (shiftEnterPressed ()), this,
           SLOT (slotShiftEnterPressed ()));
  connect (ui.cb_find->lineEdit (), SIGNAL (finished ()), this,
           SLOT (accept ()));
  ui.cb_find->lineEdit ()->setPlaceholderText (tr ("Search for ..."));
  ui.cb_find->setInputMethodHints (Qt::ImhNoAutoUppercase|Qt::ImhNoPredictiveText);

  QIcon icon = QCommonStyle().standardIcon (QStyle::SP_ArrowUp);
  QAction *a = ui.cb_find->lineEdit ()->addAction (icon, QLineEdit::TrailingPosition);
  connect (a, SIGNAL (triggered ()), this, SLOT (slotShiftEnterPressed ()));
  icon = QCommonStyle().standardIcon (QStyle::SP_ArrowDown);
  a = ui.cb_find->lineEdit ()->addAction (icon, QLineEdit::TrailingPosition);
  connect (a, SIGNAL (triggered ()), this, SLOT (slotFindNext ()));

  QSettings cfg ("qpwmc");
  restoreGeometry (cfg.value ("searchGeometry").toByteArray ());
}

PwmdSearchDialog::~PwmdSearchDialog ()
{
  QSettings cfg ("qpwmc");
  cfg.setValue ("searchGeometry", saveGeometry ());
  cfg.sync ();
}

void
PwmdSearchDialog::slotAttrChanged (int s)
{
  bool b = ui.ck_attr->isChecked () || ui.ck_attrValue->isChecked () || ui.ck_elements->isChecked ();
  ui.ck_select->setEnabled (b);
  emit findNext (ui.cb_find->lineEdit ()->text (),
                 ui.ck_elements->isChecked (),
                 s == Qt::Checked,
                 ui.ck_attrValue->isChecked (),
                 ui.ck_select->isChecked (),
                 false);
}

void
PwmdSearchDialog::slotAttrValueChanged (int s)
{
  bool b = ui.ck_attr->isChecked () || ui.ck_attrValue->isChecked () || ui.ck_elements->isChecked ();
  ui.ck_select->setEnabled (b);
  emit findNext (ui.cb_find->lineEdit ()->text (),
                 ui.ck_elements->isChecked (),
                 ui.ck_attr->isChecked (),
                 s == Qt::Checked,
                 ui.ck_select->isChecked (),
                 false);
}

void
PwmdSearchDialog::slotElementChanged (int s)
{
  bool b = ui.ck_attr->isChecked () || ui.ck_attrValue->isChecked () || ui.ck_elements->isChecked ();
  ui.ck_select->setEnabled (b);
  emit findNext (ui.cb_find->lineEdit ()->text (),
                 s == Qt::Checked,
                 ui.ck_attr->isChecked (),
                 ui.ck_attrValue->isChecked (),
                 ui.ck_select->isChecked (),
                 false);
}

void
PwmdSearchDialog::slotSelectElementsChanged (int state)
{
  bool b = false;

  b = ui.ck_attr->isChecked () || ui.ck_attrValue->isChecked () || ui.ck_elements->isChecked ();
  ui.ck_select->setEnabled (b);
  emit findNext (ui.cb_find->lineEdit ()->text (),
                 ui.ck_elements->isChecked (),
                 ui.ck_attr->isChecked (),
                 ui.ck_attrValue->isChecked (),
                 b && state == Qt::Checked,
                 false);
}

void
PwmdSearchDialog::setSearchText (QComboBox *cb, bool update)
{
  QComboBox *src = update ? ui.cb_find : cb;
  QComboBox *dst = update ? cb : ui.cb_find;
  bool m = false;

  dst->clear ();

  for (int i = 0; i < src->count (); i++)
    {
      dst->addItem (src->itemText (i));
      if (src->itemText(i) == src->currentText())
        m = true;
    }

  if (!m && !src->currentText().isEmpty ())
    {
      dst->insertItem (0, src->currentText());
      dst->setCurrentText (src->currentText ());
    }
  else
    dst->setCurrentIndex (src->currentIndex ());
}

void
PwmdSearchDialog::slotFindNext ()
{
  emit findNext (ui.cb_find->lineEdit ()->text (),
                 ui.ck_elements->isChecked (),
                 ui.ck_attr->isChecked (),
                 ui.ck_attrValue->isChecked (),
                 ui.ck_select->isChecked () && ui.ck_select->isEnabled (),
                 false);
}

void
PwmdSearchDialog::slotShiftEnterPressed ()
{
  emit findNext (ui.cb_find->lineEdit ()->text (),
                 ui.ck_elements->isChecked (),
                 ui.ck_attr->isChecked (),
                 ui.ck_attrValue->isChecked (),
                 ui.ck_select->isChecked () && ui.ck_select->isEnabled (),
                 true);
}

void
PwmdSearchDialog::reset ()
{
  ui.ck_select->setChecked (false);
}

void
PwmdSearchDialog::setIncremental (bool b)
{
  if (b)
    connect (ui.cb_find, SIGNAL (editTextChanged (const QString &)),
             this, SLOT (slotFindTextChanged (const QString &)));
  else
    disconnect (ui.cb_find, SIGNAL (editTextChanged (const QString &)),
                this, SLOT (slotFindTextChanged (const QString &)));
}

void
PwmdSearchDialog::slotFindTextChanged (const QString &str)
{
  emit findNext (str,
                 ui.ck_elements->isChecked (),
                 ui.ck_attr->isChecked (),
                 ui.ck_attrValue->isChecked (),
                 ui.ck_select->isChecked () && ui.ck_select->isEnabled (),
                 false);
}
