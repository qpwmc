/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QMessageBox>
#include "pwmdFileOptionsWidget.h"
#include "pwmdPasswordDialog.h"
#include "cmdIds.h"

static int lastCacheTimeout;

PwmdFileOptionsWidget::PwmdFileOptionsWidget (QWidget *p) : QFrame (p)
{
  ui.setupUi (this);

  _finishing = false;
  _opened = false;
  _decryptKeyFile = QString ();
  _filename = QString ();
  _sym = false;
  _sign = false;
  connect (ui.pb_changePassphrase, SIGNAL (clicked()), this,
           SLOT (slotChangePassword ()));
  connect (ui.pb_clearCache, SIGNAL (clicked ()), this,
           SLOT (slotClearCacheEntry ()));
  connect (ui.pb_updateCacheTimeout, SIGNAL (clicked ()), this,
           SLOT (slotUpdateCacheTimeout ()));
  ui.pb_updateCacheTimeout->setEnabled (false);
  connect (ui.sp_cacheTimeout, SIGNAL (valueChanged (int)), this,
           SLOT (slotCacheTimeoutChanged (int)));
  connect (ui.ck_notCached, SIGNAL (stateChanged (int)), this,
           SLOT (slotNotCachedChanged (int)));
  connect (ui.ck_noTimeout, SIGNAL (stateChanged (int)), this,
           SLOT (slotNoTimeoutChanged (int)));
}

PwmdFileOptionsWidget::~PwmdFileOptionsWidget ()
{
}

QString
PwmdFileOptionsWidget::decryptKeyFile ()
{
  return _decryptKeyFile;
}

/* 'opened' is true when called from PwmdFileOptionsDialog. */
void
PwmdFileOptionsWidget::setFilename (QString s, bool opened)
{
  _filename = s;
  ui.l_filename->setText (s.isEmpty () ? "-" : _filename);

  if (!opened)
    {
      ui.pb_changePassphrase->setText (tr ("&Key file..."));
      if (s.isEmpty ())
        return;
    }

  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdFileGetConfig, "GETCONFIG",
                                          Pwmd::inquireCallback,
                                          new PwmdInquireData (QString ("%1 cache_timeout").arg (_filename))),
                                          true);

  PwmdInquireData *inq = new PwmdInquireData (_filename);
  PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdFileIsCached,
                                                         "ISCACHED",
                                                         Pwmd::inquireCallback, inq);
  item->addError (GPG_ERR_SOURCE_USER_1, GPG_ERR_ENOENT);
  pwm->command (item, true);

  if (opened)
    {
      _opened = true;
      connect (ui.ck_lock, SIGNAL (stateChanged (int)), this,
               SLOT (slotLock (int)));

      item = new PwmdCommandQueueItem (PwmdCmdIdFileKeyInfo, "KEYINFO",
                                       Pwmd::inquireCallback, 0);
      item->addError (GPG_ERR_SOURCE_USER_1, GPG_ERR_NO_DATA);
      pwm->command (item, true);

      pwm->command (new PwmdCommandQueueItem (PwmdCmdIdFileClientInfo,
                                              "GETINFO", Pwmd::inquireCallback,
                                              new PwmdInquireData ("--verbose CLIENTS")));
    }
  else
    pwm->runQueue ();

  ui.sp_cacheTimeout->setEnabled (pwm->state () == Pwmd::Opened
                                  && pwm->filename () == s);
  ui.ck_noTimeout->setEnabled (ui.sp_cacheTimeout->isEnabled ());
  ui.ck_notCached->setEnabled (ui.sp_cacheTimeout->isEnabled ());
  ui.pb_updateCacheTimeout->setEnabled (ui.sp_cacheTimeout->isEnabled ());
}

bool
PwmdFileOptionsWidget::lock ()
{
  return ui.ck_lock->isChecked ();
}

void
PwmdFileOptionsWidget::slotLock (int n)
{
  bool b = n == Qt::Checked ? true : false;
  PwmdInquireData *inq = new PwmdInquireData();
  inq->setUserBool (b);
  PwmdCommandQueueItem *cmd = new PwmdCommandQueueItem (PwmdCmdIdFileLock,
                                                        b ? "LOCK" : "UNLOCK",
                                                        0, inq);
  if (!b)
    cmd->addError (GPG_ERR_SOURCE_USER_1, GPG_ERR_NOT_LOCKED);

  if (!pwm->command (cmd))
    delete inq;
}

void
PwmdFileOptionsWidget::setHandle (Pwmd *p)
{
  pwm = p;
  connect (pwm, SIGNAL (statusMessage (QString, void *)), this, SLOT (slotStatusMessage (QString, void *)));
  connect (pwm, SIGNAL (commandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)), this, SLOT (slotCommandResult (PwmdCommandQueueItem *, QString, gpg_error_t, bool)));
}

void
PwmdFileOptionsWidget::setLocked (bool b)
{
  ui.ck_lock->disconnect (this);
  ui.ck_lock->setChecked (b);
  connect (ui.ck_lock, SIGNAL (stateChanged (int)), this, SLOT (slotLock (int)));
}

void
PwmdFileOptionsWidget::isCachedFinalize (gpg_error_t rc)
{
  if (gpg_err_code (rc) == GPG_ERR_ENOENT)
    ui.pb_changePassphrase->setEnabled (false);

  ui.l_cached->setText (rc ? tr ("no") : tr ("yes"));
  ui.pb_clearCache->setEnabled (rc ? false : true);
}

void
PwmdFileOptionsWidget::cacheTimeoutFinalize (const QString &result)
{
  int n = result.toInt();

  lastCacheTimeout = n;
  ui.sp_cacheTimeout->setValue (n);
  slotCacheTimeoutChanged (n);
}

static void
showErrorMsg (gpg_error_t rc, const QString &str)
{
  QMessageBox m;

  m.setText (QApplication::
	     tr ("There was an error while communicating with pwmd."));
  m.setInformativeText (QString ("ERR %1: %2: %3").arg (rc).arg (gpg_strsource (rc), str));
  m.setIcon (QMessageBox::Critical);
  m.exec ();
}

void
PwmdFileOptionsWidget::refreshClientInfoFinalize (const QString &line)
{
  QStringList list = line.split ("\n");
  unsigned t = list.count();

  for (unsigned i = 0; i < t; i++)
    {
      if (list.at (i).isEmpty())
        continue;

      ClientInfo *ci = ClientInfo::parseLine (list.at (i));
      if (ci->filename () == _filename && ci->self ())
        {
          ui.ck_lock->disconnect (this);
          ui.ck_lock->setChecked (ci->locked ());
          connect (ui.ck_lock, SIGNAL (stateChanged (int)), this, SLOT (slotLock (int)));
          delete ci;
          break;
        }

      delete ci;
    }
}

void
PwmdFileOptionsWidget::slotCommandResult (PwmdCommandQueueItem *item,
                                          QString result, gpg_error_t rc,
                                          bool queued)
{
  if ((item->id () >= 0 && item->id () <= PwmdCmdIdClientMax)
      || item->id () >= PwmdCmdIdFileMax)
    {
      item->setSeen ();
      return;
    }

  switch (item->id ())
    {
    case PwmdCmdIdInternalConnect:
    case PwmdCmdIdInternalConnectHost:
    case PwmdCmdIdInternalOpen:
    case PwmdCmdIdInternalSave:
      item->setSeen ();
      return;
    case PwmdCmdIdInternalPassword:
      (void)pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 0);
      (void)pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 0);
      break;
    case PwmdCmdIdFileIsCached:
      isCachedFinalize (rc);
      break;
    case PwmdCmdIdFileGetConfig:
      cacheTimeoutFinalize (result);
      break;
    case PwmdCmdIdFileCacheTimeout:
      updateCacheTimeoutFinalize (rc);
      break;
    case PwmdCmdIdFileClearCache:
      clearCacheEntryFinalize (rc);
      break;
    case PwmdCmdIdFileKeyInfo:
      keyInfoFinalize (rc, result);
      break;
    case PwmdCmdIdFileClientInfo:
      if (!rc)
        refreshClientInfoFinalize (result);
      break;
    case PwmdCmdIdFileLock:
      if (rc && gpg_err_code (rc) != GPG_ERR_NOT_LOCKED)
        {
          ui.ck_lock->disconnect (this);
          ui.ck_lock->setChecked (!ui.ck_lock->isChecked ());
          connect (ui.ck_lock, SIGNAL (stateChanged (int)), this, SLOT (slotLock (int)));
        }

      emit clientLockStateChanged (ui.ck_lock->isChecked ());
      break;
    default:
      break;
    }

  if (rc && !item->checkError (rc) && !queued)
    {
      if (item->error () && !item->errorString().isEmpty ())
        showErrorMsg (item->error (), item->errorString ());
      else
        Pwmd::showError (rc, pwm);
    }

  item->setSeen ();
}

void
PwmdFileOptionsWidget::keyInfoFinalize (gpg_error_t rc, const QString &result)
{
  _sym = true;
  _sign = false;
  QStringList list;

  if (gpg_err_code (rc) == GPG_ERR_NO_DATA)
    goto done;

  list = result.split ("\n");

  for (int n = 0; n < list.count(); n++)
    {
      if (list.at(n).at(0) != 'S')
        _sym = false;
      else
        _sign = true;
    }

done:
  ui.pb_changePassphrase->setEnabled (!_sym);
}

void
PwmdFileOptionsWidget::slotStatusMessage (QString line, void *data)
{
  (void)data;
  if (line.contains ("CACHE"))
    {
      PwmdCommandQueueItem *item = new PwmdCommandQueueItem (PwmdCmdIdFileIsCached, QString("ISCACHED %1").arg (pwm->filename()));
      item->addError (GPG_ERR_SOURCE_USER_1, GPG_ERR_ENOENT);
      pwm->command (item);
    }
}

void
PwmdFileOptionsWidget::clearCacheEntryFinalize (gpg_error_t rc)
{
  if (!rc)
    ui.l_cached->setText (tr ("no"));
  ui.pb_clearCache->setEnabled (!rc ? false : true);
}

void
PwmdFileOptionsWidget::slotClearCacheEntry ()
{
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdFileClearCache, QString ("CLEARCACHE %1").arg (_filename)));
}

void
PwmdFileOptionsWidget::updateCacheTimeoutFinalize (gpg_error_t rc)
{
  if (!rc)
    {
      lastCacheTimeout = ui.sp_cacheTimeout->value ();
      ui.ck_notCached->setChecked (ui.sp_cacheTimeout->value () == 0);
      ui.ck_noTimeout->setChecked (ui.sp_cacheTimeout->value () == -1);
    }

  ui.pb_updateCacheTimeout->setEnabled (!!rc);
  if (_finishing)
    emit commandsFinished ();
}

void
PwmdFileOptionsWidget::slotUpdateCacheTimeout ()
{
  pwm->command (new PwmdCommandQueueItem (PwmdCmdIdFileCacheTimeout, QString ("CACHETIMEOUT %1").arg (ui.sp_cacheTimeout->value())));
}

bool
PwmdFileOptionsWidget::updateCacheTimeout ()
{
  if (ui.pb_updateCacheTimeout->isEnabled ())
    {
      _finishing = true;
      slotUpdateCacheTimeout ();
      return false;
    }

  return true;
}

void
PwmdFileOptionsWidget::slotCacheTimeoutChanged (int n)
{
  ui.pb_updateCacheTimeout->setEnabled (lastCacheTimeout != n);
  ui.ck_noTimeout->setChecked (n == -1);
  ui.ck_notCached->setChecked (n == 0);
}

void
PwmdFileOptionsWidget::slotNotCachedChanged (int n)
{
  bool b = n == Qt::Checked ? true : false;

  ui.ck_noTimeout->setChecked (!b && ui.sp_cacheTimeout->value () == -1);
  if (b)
    ui.sp_cacheTimeout->setValue (0);
}

void
PwmdFileOptionsWidget::slotNoTimeoutChanged (int n)
{
  bool b = n == Qt::Checked ? true : false;

  ui.ck_notCached->setChecked (!b && ui.sp_cacheTimeout->value () == 0);
  if (b)
    ui.sp_cacheTimeout->setValue (-1);
}

/* Also used to select a filename during PwmdOpenDialog. */
void
PwmdFileOptionsWidget::slotChangePassword ()
{
  PwmdPasswordDialog *d = new PwmdPasswordDialog (this);

  if (!_opened)
    d->setDecryptKeyFile (_decryptKeyFile);

  d->setSymmetric (_sym);
  d->setHasSigners (_sign);
  d->setNewFile (false);

  if (d->exec ())
    {
      gpg_error_t rc;

      if (d->useKeyFile ())
        {
          if (!_opened)
            {
              _decryptKeyFile = d->decryptKeyFile ();
              delete d;
              return;
            }

          PwmdInquireData *inq = new PwmdInquireData;

          inq->setHandle (pwm->handle());
          inq->setKeyFile (d->decryptKeyFile ());
          
          if (!_sym)
            inq->setEncryptKeyFile (d->signKeyFile ());
          else
            inq->setEncryptKeyFile (d->encryptKeyFile ());

          inq->setSignKeyFile (d->signKeyFile ());
          rc = pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 1);
          if (!rc)
            rc = pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 1);

          if (!rc)
            pwm->passwd (0, Pwmd::inquireCallback, inq);
        }
      else
        {
          rc = pwmd_setopt (pwm->handle (), PWMD_OPTION_LOCAL_PINENTRY, 0);
          if (!rc)
            rc = pwmd_setopt (pwm->handle (), PWMD_OPTION_OVERRIDE_INQUIRE, 0);

          if (!rc)
            pwm->passwd ();
        }

      if (rc)
        Pwmd::showError (rc, pwm);
    }

  delete d;
}
