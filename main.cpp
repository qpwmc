/*
    Copyright (C) 2010-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QApplication>
#include <QSettings>
#include <QFile>
#include <iostream>
#include <libpwmd.h>
#ifdef Q_OS_ANDROID
#include <stdlib.h>
#endif
#include "main.h"
#include "applicationForm.h"
#include "pwmdMainWindow.h"

#ifndef Q_OS_ANDROID
#include "trayIcon.h"

static void
usage (int e)
{
  qWarning ("%s", QApplication::tr (
	    "Usage: qpwmc [-hvyn] [-d <FD>] [-p <path>] [-s <socket | remote>]\n"
            "             [-e <datafile>] [-f <form>]\n"
	    "  -s <socket>    alternate socket or remote name to connect to\n"
	    "  -e <filename>  edit the specified data filename\n"
	    "  -f <filename>  load a qpwmc application form\n"
            "  -d <fd>        write connection info to the specified file descriptor\n"
            "  -p <path>      initial element path\n"
            "  -y             start in the system tray or after editing (-e)\n"
            "  -n             don't lock the data file\n"
	    "  -h             this help text\n"
	    "  -v             version information").toUtf8().data());
  exit (e);
}

static bool
buildResultLine (QString sock, QString file, QString path, QByteArray &result)
{
  QByteArray b;
  PwmdRemoteHost host;
  QString tmp;

  if (PwmdRemoteHost::fillRemoteHost (sock, host))
    {
      b.append(QString("<SOCKET>%1:%2").arg(sock.length()).arg(sock).toUtf8 ());

      for (int i = 0; i < host.socketArgs().length(); i++)
        {
          QString s = host.socketArgs ().at (i);

          tmp = QString ("<SOCKET-ARG>%2:%3").arg (s.length()).arg (s);
          b.append (tmp.toUtf8 ());
        }

      if (host.type () == PWMD_SOCKET_SSH)
        {
          tmp = QString ("<SSH-AGENT>1:%1").arg (host.sshAgent ());
          b.append (tmp.toUtf8 ());
        }
      else if (host.type () == PWMD_SOCKET_TLS)
        {
          tmp = QString ("<TLS-VERIFY>1:%1").arg (host.tlsVerify ());
          b.append (tmp.toUtf8 ());
          tmp = QString ("<TLS-PRIORITY>%1:%2").arg (host.tlsPriority().length()).arg (host.tlsPriority ());
          b.append (tmp.toUtf8 ());
        }

      QString s = QString::number (host.connectTimeout ());
      tmp = QString ("<CONNECT-TIMEOUT>%1:%2").arg (s.length ()).arg (s);
      b.append (tmp.toUtf8 ());

      s = QString::number (host.socketTimeout ());
      tmp = QString ("<SOCKET-TIMEOUT>%1:%2").arg (s.length ()).arg (s);
      b.append (tmp.toUtf8 ());
    }
  else
    b.append(QString("<SOCKET>%1:%2").arg(sock.length()).arg(sock).toUtf8 ());

  b.append(QString("<FILE>%1:%2").arg(file.length()).arg(file).toUtf8 ());
  b.append(QString("<PATH>%1:%2").arg(path.length()).arg(path).toUtf8 ());
  result = b;
  return true;
}
#endif

#ifndef Q_OS_ANDROID
static int
startsInTray (QApplication &app)
{
  TrayIcon *sti = new TrayIcon ();
  sti->show ();
  app.setQuitOnLastWindowClosed (false);
  return app.exec ();
}
#endif

int
main (int argc, char **argv)
{
#ifdef Q_OS_ANDROID
  setenv ("QT_NECESSITAS_COMPATIBILITY_LONG_PRESS", "1", 1);
  setenv ("QT_ANDROID_ENABLE_RIGHT_MOUSE_FROM_LONG_PRESS", "1", 1);
#endif
  QApplication app (argc, argv);
  int n = 0;
#ifndef Q_OS_ANDROID
  QString filename = QString ();
  QString socket = QString ();
  QString templateFile = QString ();
  QStringList args = app.arguments ();
  bool editOnly = false;
  int fileDescriptor = -1;
  QString path = QString ();
  QFile outFile;
  QSettings cfg ("qpwmc");
  bool tray, trayArg = false;
  bool lock = true;
#endif

  app.setWindowIcon(QIcon(":icons/trayicon.svg"));

#ifndef Q_OS_ANDROID
  tray = trayArg = false;

  for (int i = 1; i < args.count (); i++)
    {
      QString arg = args.at (i);

      if (arg.at (0) == '-')
	{
	  switch (arg.at (1).toLatin1 ())
	    {
            case 'y':
              tray = trayArg = true;
              break;
	    case 's':
	      socket = args.at (++i);
	      break;
	    case 'e':
	      editOnly = true;
              if (i+1 < args.count () && !args.at (i+1).isEmpty ()
                  && args.at (i+1).at (0) != '-')
                filename = args.at (++i);
	      break;
	    case 't':
            case 'f':
	      templateFile = args.at (++i);
	      break;
	    case 'v':
	      qWarning ("QPwmc %s\nlibpwmd %s", QPWMC_VERSION,
                        pwmd_version ());
	      exit (0);
            case 'd':
              fileDescriptor = args.at (++i).toInt();
              if (!outFile.open (fileDescriptor, QIODevice::WriteOnly,
                                QFile::AutoCloseHandle))
                {
                  qWarning ("Could not open FD for writing. Exiting.");
                  exit (EXIT_FAILURE);
                }
              break;
            case 'p':
              path = args.at (++i);
              break;
            case 'n':
              lock = false;
              break;
	    case 'h':
	    default:
	      usage (EXIT_SUCCESS);
	    }
	}
    }

  if (!tray)
    tray = cfg.value ("startsInTray", false).toBool ();

  if (tray && templateFile.isEmpty () && !editOnly)
    return startsInTray (app);
  else if (!templateFile.isEmpty ())
    {
      ApplicationForm *a = new ApplicationForm (templateFile, socket, filename,
                                                path);
      n = a->hasError ();
      if (!n)
        {
	  a->setParent (0, Qt::Window);
          a->exec ();
          n = a->hasError ();
          if (!n && fileDescriptor != -1)
            {
              QByteArray b;

              buildResultLine (a->socket(), a->filename(), a->elementPath (), b);
              outFile.write (b);
            }

          n = a->hasError ();
        }

      delete a;
    }
  else
    {
      PwmdMainWindow *w = new PwmdMainWindow (filename, "qpwmc", lock, path,
                                              socket, 0);
      w->show();
      n = app.exec ();

      if (!n && fileDescriptor != -1)
        {
          QByteArray b;

          buildResultLine (w->socket(), w->filename(),
                           w->elementPath (), b);
          outFile.write (b);
        }

      delete w;

      if (fileDescriptor == -1)
        {
          cfg.sync ();
          tray = cfg.value ("startsInTray", false).toBool ();
          if (!editOnly && (trayArg || tray))
            return startsInTray (app);
        }
    }
#else
  PwmdMainWindow *w = new PwmdMainWindow ();
  w->show();
  n = app.exec ();
  delete w;
#endif

  return n;
}
