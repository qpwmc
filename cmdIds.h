/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef CMDIDS_H 
#define CMDIDS_H

/* PwmdMainWindow */
#define PwmdCmdIdCommand 		0
#define PwmdCmdIdElementTree		1
#define PwmdCmdIdLock			2
#define PwmdCmdIdStoreContent		3
#define PwmdCmdIdPasswordContent	4
#define PwmdCmdIdAttrContent		5
#define PwmdCmdIdAttrNew		6
#define PwmdCmdIdAttrDelete		7
#define PwmdCmdIdAttrList		8
#define PwmdCmdIdNewElement		9
#define PwmdCmdIdDeleteElement		10
#define PwmdCmdIdRenameElement		11
#define PwmdCmdIdInvokingUser		12
#define PwmdCmdIdCurrentUser		13
#define PwmdCmdIdReload			14
#define PwmdCmdIdGetContent		15
#define PwmdCmdIdDndCopyElement		16
#define PwmdCmdIdDndMoveElement		17
#define PwmdCmdIdDndCreateTarget	18
#define PwmdCmdIdInsertContent		19
#define PwmdCmdIdElementTreeForm	20
#define PwmdCmdIdClose			21
#define PwmdCmdIdLockTimeout		22
#define PwmdCmdIdUnLock			23
#define PwmdCmdIdMainMax		100

/* PwmdOpenDialog */
#define PwmdCmdIdOpenIsCached		101
#define PwmdCmdIdOpenGetConfig		102
#define PwmdCmdIdOpenLs			103
#define PwmdCmdIdOpenClearCache		104
#define PwmdCmdIdOpenCacheTimeout	105
#define PwmdCmdIdOpenMax		120

/* PwmdSaveDialog */
#define PwmdCmdIdSaveKeyInfo		121
#define PwmdCmdIdSaveListKeys		122
#define PwmdCmdIdSaveLearn		123
#define PwmdCmdIdSaveDeleteKey		124
#define PwmdCmdIdSaveMax		140

/* PwmdClientDialog */
#define PwmdCmdIdClientList		141
#define PwmdCmdIdClientKill		142
#define PwmdCmdIdClientState		143
#define PwmdCmdIdClientNCache		144
#define PwmdCmdIdClientNClients		145
#define PwmdCmdIdClientMax		150

/* PwmdFileOptionsDialog */
#define PwmdCmdIdFileGetConfig		151
#define PwmdCmdIdFileIsCached		152
#define PwmdCmdIdFileClearCache		153
#define PwmdCmdIdFileCacheTimeout	154
#define PwmdCmdIdFileKeyInfo		155
#define PwmdCmdIdFileLock		156
#define PwmdCmdIdFileClientInfo		157
#define PwmdCmdIdFileMax		160

/* ApplicationForm */
#define PwmdCmdIdFormGetContent		161
#define PwmdCmdIdFormStoreContent	162
#define PwmdCmdIdFormAttrNew		163

#endif
