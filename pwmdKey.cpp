/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include "pwmdKey.h"

PwmdKey::PwmdKey (bool revoked, bool expired, bool disabled, bool invalid,
                  bool encrypt, bool sign, bool certify, bool secret,
                  bool auth, bool qualified, int protocol, int trust)
{
  _revoked = revoked;
  _expired = expired;
  _disabled = disabled;
  _invalid = invalid;
  _canEncrypt = encrypt;
  _canSign = sign;
  _canCertify = certify;
  _secret = secret;
  _canAuthenticate = auth;
  _qualified = qualified;
  _protocol = protocol;
  _issuerSerial = QString ();
  _issuerName = QString ();
  _chainId = QString ();
  _ownerTrust = trust;
};

PwmdKey::~PwmdKey()
{
  while (!_subKeys.isEmpty ())
    {
      PwmdSubKey *key = _subKeys.takeFirst ();
      delete key;
    }

  while (!_userIds.isEmpty ())
    {
      PwmdUserId *id = _userIds.takeFirst ();
      delete id;
    }
}

void
PwmdKey::setUserIds (QList <PwmdUserId *> list)
{
  _userIds = list;
};

QList <PwmdUserId *>
PwmdKey::userIds()
{
  return _userIds;
}

void
PwmdKey::setSubkeys (QList <PwmdSubKey *> list)
{
  _subKeys = list;
}

QList <PwmdSubKey *>
PwmdKey::subKeys()
{
  return _subKeys;
}

bool
PwmdKey::isRevoked()
{
  return _revoked;
}

void
PwmdKey::setRevoked (bool b)
{
  _revoked = b;
}

bool
PwmdKey::isExpired ()
{
  return _expired;
}

void
PwmdKey::setExpired (bool b)
{
  _expired = b;
}

bool
PwmdKey::isDisabled()
{
  return _disabled;
}

void
PwmdKey::setDisabled (bool b)
{
  _disabled = b;
}

bool
PwmdKey::isInvalid()
{
  return _invalid;
}

void
PwmdKey::setInvalid (bool b)
{
  _invalid = b;
}

bool
PwmdKey::canEncrypt()
{
  return _canEncrypt;
}

void
PwmdKey::setCanEncrypt (bool b)
{
  _canEncrypt = b;
}

bool
PwmdKey::canSign()
{
  return _canSign;
}

void
PwmdKey::setCanSign (bool b)
{
  _canSign = b;
}

bool
PwmdKey::canCertify()
{
  return _canCertify;
}

void
PwmdKey::setCanCertify (bool b)
{
  _canCertify = b;
}

bool
PwmdKey::isSecret()
{
  return _secret;
}

void
PwmdKey::setSecret (bool b)
{
  _secret = b;
}

bool
PwmdKey::canAuthenticate()
{
  return _canAuthenticate;
}

void
PwmdKey::setCanAuthenticate (bool b)
{
  _canAuthenticate = b;
}

bool
PwmdKey::isQualified()
{
  return _qualified;
}

void
PwmdKey::setQualified (bool b)
{
  _qualified = b;
}

int
PwmdKey::protocol()
{
  return _protocol;
}

void
PwmdKey::setProtocol(int n)
{
  _protocol = n;
}

QString
PwmdKey::issuerSerial()
{
  return _issuerSerial;
}

void
PwmdKey::setIssuerSerial (QString s)
{
  _issuerSerial = s;
}

QString
PwmdKey::issuerName()
{
  return _issuerName;
}

void
PwmdKey::setIssuerName (QString s)
{
  _issuerName = s;
}

QString
PwmdKey::chainId()
{
  return _chainId;
}

void
PwmdKey::setChainId(QString s)
{
  _chainId = s;
}

int
PwmdKey::ownerTrust()
{
  return _ownerTrust;
}

void
PwmdKey::setOwnerTrust (int n)
{
  _ownerTrust = n;
}

PwmdSubKey::PwmdSubKey () : PwmdKey()
{
  _isCardKey = false;
  _pubkeyAlgo = 0;
  _nbits = 0;
  _keyId = QString ();
  _fingerprint = QString ();
  _keygrip = QString ();
  _timestamp = 0;
  _expires = 0;
  _cardNumber = QString ();
  _eccCurve = QString ();
}

PwmdSubKey::~PwmdSubKey()
{
}

bool
PwmdSubKey::isCardKey()
{
  return _isCardKey;
}

void
PwmdSubKey::setCardKey(bool b)
{
  _isCardKey = b;
}

int
PwmdSubKey::pubkeyAlgo()
{
  return _pubkeyAlgo;
}

void
PwmdSubKey::setPubkeyAlgo(int n)
{
  _pubkeyAlgo = n;
}

void
PwmdSubKey::setNBits (unsigned n)
{
  _nbits = n;
}

unsigned
PwmdSubKey::nBits ()
{
  return _nbits;
}

QString
PwmdSubKey::keyId()
{
  return _keyId;
}

void
PwmdSubKey::setKeyId (QString s)
{
  _keyId = s;
}

QString
PwmdSubKey::fingerprint()
{
  return _fingerprint;
}

void
PwmdSubKey::setFingerprint (QString s)
{
  _fingerprint = s;
}

QString
PwmdSubKey::keygrip()
{
  return _keygrip;
}

void
PwmdSubKey::setKeygrip (QString s)
{
  _keygrip = s;
}

long
PwmdSubKey::created()
{
  return _timestamp;
}

void
PwmdSubKey::setCreated (long n)
{
  _timestamp = n;
}

long
PwmdSubKey::expires()
{
  return _expires;
}

void
PwmdSubKey::setExpires (long n)
{
  _expires = n;
}

QString
PwmdSubKey::cardNumber()
{
  return _cardNumber;
}

void
PwmdSubKey::setCardNumber (QString s)
{
  _cardNumber = s;
}

QString
PwmdSubKey::curve()
{
  return _eccCurve;
}

void
PwmdSubKey::setCurve (QString s)
{
  _eccCurve = s;
}

PwmdUserId::PwmdUserId ()
{
  _revoked = false;
  _invalid = false;
  _validity = 0;
  _userId = QString ();
  _name = QString ();
  _email = QString ();
  _comment = QString ();
}

PwmdUserId::~PwmdUserId()
{
}

bool
PwmdUserId::isRevoked()
{
  return _revoked;
}

void
PwmdUserId::setRevoked (bool b)
{
  _revoked = b;
}

bool
PwmdUserId::isInvalid()
{
  return _invalid;
}

void
PwmdUserId::setInvalid (bool b)
{
  _invalid = b;
}

int
PwmdUserId::validity()
{
  return _validity;
}

void
PwmdUserId::setValidity (int n)
{
  _validity = n;
}

QString
PwmdUserId::userId()
{
  return _userId;
}

void
PwmdUserId::setUserId(QString s)
{
  _userId = s;
}

QString
PwmdUserId::name()
{
  return _name;
}

void
PwmdUserId::setName (QString s)
{
  _name = s;
}

QString
PwmdUserId::email()
{
  return _email;
}

void
PwmdUserId::setEmail (QString s)
{
  _email = s;
}

QString
PwmdUserId::comment()
{
  return _comment;
}

void
PwmdUserId::setComment (QString s)
{
  _comment = s;
}

PwmdKeyItemData::PwmdKeyItemData (PwmdKey *k, PwmdSubKey *s)
{
  _key = k;
  _subKey = s;
}

PwmdKeyItemData::~PwmdKeyItemData ()
{
  if (_key)
    delete _key;

  if (_subKey)
    delete _subKey;
}

void
PwmdKeyItemData::setKey (PwmdKey *k)
{
  _key = k;
}

PwmdKey *
PwmdKeyItemData::key ()
{
  return _key;
}

void
PwmdKeyItemData::setSubKey (PwmdSubKey *k)
{
  _subKey = k;
}

PwmdSubKey *
PwmdKeyItemData::subKey ()
{
  return _subKey;
}
