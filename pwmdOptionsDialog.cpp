/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QSettings>
#include <QPushButton>
#include <QColorDialog>
#include <QScroller>
#include "pwmdOptionsDialog.h"

PwmdOptionsDialog::PwmdOptionsDialog (QWidget *p) : QDialog (p)
{
  QSettings cfg ("qpwmc");
  ui.setupUi (this);

#ifdef Q_OS_ANDROID
  ui.ck_requireUnlock->setChecked (cfg.value ("requireUnlock", true).toBool ());
#endif
  ui.ck_startsInTray->setChecked (cfg.value ("startsInTray", false).toBool ());
  ui.ck_sortElementTree->setChecked (cfg.value ("sortElementTree", true).toBool ());
  ui.ck_executeOnEnter->setChecked (cfg.value ("executeOnEnter", true).toBool ());
  ui.ck_updateOnEnter->setChecked (cfg.value ("updateOnEnter", true).toBool ());
  ui.ck_incrementalSearch->setChecked (cfg.value ("incrementalSearch", true).toBool ());
  ui.ck_confirmQuit->setChecked (cfg.value ("confirmQuit", true).toBool ());
  ui.sp_clipboardTimeout->setValue (cfg.value ("clipboardTimeout", 20).
				    toInt ());
  ui.sp_lockTimeout->setValue (cfg.value ("lockTimeout", 5).toInt ());
  ui.sp_releaseTimeout->setValue (cfg.value ("releaseTimeout", 0).toInt ());
  ui.ck_reopenLastFile->setChecked (cfg.value ("reopenLastFile", false).
				    toBool ());
  targetColor = cfg.value ("targetColor", "DarkOrange").value < QColor > ();
  permissionsColor = cfg.value ("permissionsColor", "DarkRed").value < QColor > ();
  invalidTargetColor = cfg.value ("invalidTargetColor", "Red").value < QColor > ();
  hilightColor = cfg.value ("hilightColor", "Green").value < QColor > ();
  targetLoopColor = cfg.value ("targetLoopColor", "DarkCyan").value < QColor > ();
  targetColorBg = cfg.value ("targetColorBg", "DarkGray").value < QColor > ();
  permissionsColorBg = cfg.value ("permissionsColorBg", "DarkGray").value < QColor > ();
  invalidTargetColorBg = cfg.value ("invalidTargetColorBg", "DarkGray").value < QColor > ();
  hilightColorBg = cfg.value ("hilightColorBg", "DarkGray").value < QColor > ();
  targetLoopColorBg = cfg.value ("targetLoopColorBg", "DarkGray").value < QColor > ();
  ui.ck_cacheContent->setChecked (cfg.value ("cacheContent", true).toBool ());
  ui.ck_geometry->setChecked (cfg.value ("rememberGeometry", true).toBool ());

  ui.pb_targetColor->setStyleSheet (QString ("background-color: %1;").arg (targetColor.name ()));
  ui.pb_permissionsColor->setStyleSheet (QString ("background-color: %1;").arg (permissionsColor.name ()));
  ui.pb_invalidTargetColor->setStyleSheet (QString ("background-color: %1;").arg (invalidTargetColor.name ()));
  ui.pb_hilightColor->setStyleSheet (QString ("background-color: %1;").arg (hilightColor.name ()));
  ui.pb_targetLoopColor->setStyleSheet (QString ("background-color: %1;").arg (targetLoopColor.name ()));
  ui.pb_targetColorBg->setStyleSheet (QString ("background-color: %1;").arg (targetColorBg.name ()));
  ui.pb_permissionsColorBg->setStyleSheet (QString ("background-color: %1;").arg (permissionsColorBg.name ()));
  ui.pb_invalidTargetColorBg->setStyleSheet (QString ("background-color: %1;").arg (invalidTargetColorBg.name ()));
  ui.pb_hilightColorBg->setStyleSheet (QString ("background-color: %1;").arg (hilightColorBg.name ()));
  ui.pb_targetLoopColorBg->setStyleSheet (QString ("background-color: %1;").arg (targetLoopColorBg.name ()));
  ui.sp_constraints->setValue (cfg.value ("constraintsFreq", 30).toInt ());

  connect (ui.pb_targetColor, SIGNAL (clicked ()), this,
	   SLOT (slotChooseTargetColor ()));
  connect (ui.pb_permissionsColor, SIGNAL (clicked ()), this,
	   SLOT (slotChoosePermissionColor ()));
  connect (ui.pb_invalidTargetColor, SIGNAL (clicked ()), this,
	   SLOT (slotChooseInvalidTargetColor ()));
  connect (ui.pb_hilightColor, SIGNAL (clicked ()), this,
	   SLOT (slotChooseHilightColor ()));
  connect (ui.pb_targetLoopColor, SIGNAL (clicked ()), this,
	   SLOT (slotChooseTargetLoopColor ()));
  connect (ui.pb_targetColorBg, SIGNAL (clicked ()), this,
	   SLOT (slotChooseTargetColorBg ()));
  connect (ui.pb_permissionsColorBg, SIGNAL (clicked ()), this,
	   SLOT (slotChoosePermissionColorBg ()));
  connect (ui.pb_invalidTargetColorBg, SIGNAL (clicked ()), this,
	   SLOT (slotChooseInvalidTargetColorBg ()));
  connect (ui.pb_hilightColorBg, SIGNAL (clicked ()), this,
	   SLOT (slotChooseHilightColorBg ()));
  connect (ui.pb_targetLoopColorBg, SIGNAL (clicked ()), this,
	   SLOT (slotChooseTargetLoopColorBg ()));

  QScroller::grabGesture (ui.scrollArea, QScroller::TouchGesture);
#ifdef Q_OS_ANDROID
  ui.l_startsInTray->setHidden (true);
  ui.ck_startsInTray->setHidden (true);
  showMaximized();
#else
  ui.l_requireUnlock->setHidden (true);
  ui.ck_requireUnlock->setHidden (true);
#endif
  writeChanges = true;
}

PwmdOptionsDialog::~PwmdOptionsDialog ()
{
  if (!writeChanges)
    return;

  QSettings cfg ("qpwmc");
  cfg.setValue ("executeOnEnter", ui.ck_executeOnEnter->isChecked ());
  cfg.setValue ("updateOnEnter", ui.ck_updateOnEnter->isChecked ());
  cfg.setValue ("incrementalSearch", ui.ck_incrementalSearch->isChecked ());
  cfg.setValue ("confirmQuit", ui.ck_confirmQuit->isChecked ());
  // FIXME  cfg.setValue ("defaultSocket", ui.socketWidget->ui.le_socket->text ());
  cfg.setValue ("reopenLastFile", ui.ck_reopenLastFile->isChecked ());
  cfg.setValue ("invalidTargetColor", invalidTargetColor);
  cfg.setValue ("targetLoopColor", targetLoopColor);
  cfg.setValue ("targetColor", targetColor);
  cfg.setValue ("permissionsColor", permissionsColor);
  cfg.setValue ("hilightColor", hilightColor);
  cfg.setValue ("invalidTargetColorBg", invalidTargetColorBg);
  cfg.setValue ("targetLoopColorBg", targetLoopColorBg);
  cfg.setValue ("targetColorBg", targetColorBg);
  cfg.setValue ("permissionsColorBg", permissionsColorBg);
  cfg.setValue ("hilightColorBg", hilightColorBg);
  cfg.setValue ("cacheContent", ui.ck_cacheContent->isChecked ());
  cfg.setValue ("rememberGeometry", ui.ck_geometry->isChecked ());
  cfg.setValue ("clipboardTimeout", ui.sp_clipboardTimeout->value ());
  cfg.setValue ("lockTimeout", ui.sp_lockTimeout->value ());
  cfg.setValue ("releaseTimeout", ui.sp_releaseTimeout->value ());
  cfg.setValue ("sortElementTree", ui.ck_sortElementTree->isChecked ());
  cfg.setValue ("startsInTray", ui.ck_startsInTray->isChecked ());
  cfg.setValue ("constraintsFreq", ui.sp_constraints->value ());
#ifdef Q_OS_ANDROID
  cfg.setValue ("requireUnlock", ui.ck_requireUnlock->isChecked ());
#endif
}

void
PwmdOptionsDialog::reject ()
{
  writeChanges = false;
  QDialog::reject ();
}

void
PwmdOptionsDialog::slotChooseHilightColor ()
{
  QColor c = QColorDialog::getColor (hilightColor);

  if (!c.isValid ())
    return;

  hilightColor.setRgba (c.rgba ());
  ui.pb_hilightColor->setStyleSheet (QString ("background-color: %1;").arg (hilightColor.name ()));
}

void
PwmdOptionsDialog::slotChooseHilightColorBg ()
{
  QColor c = QColorDialog::getColor (hilightColorBg);

  if (!c.isValid ())
    return;

  hilightColorBg.setRgba (c.rgba ());
  ui.pb_hilightColorBg->setStyleSheet (QString ("background-color: %1;").arg (hilightColorBg.name ()));
}

void
PwmdOptionsDialog::slotChooseTargetColor ()
{
  QColor c = QColorDialog::getColor (targetColor);

  if (!c.isValid ())
    return;

  targetColor.setRgba (c.rgba ());
  ui.pb_targetColor->setStyleSheet (QString ("background-color: %1;").arg (targetColor.name ()));
}

void
PwmdOptionsDialog::slotChooseTargetColorBg ()
{
  QColor c = QColorDialog::getColor (targetColorBg);

  if (!c.isValid ())
    return;

  targetColorBg.setRgba (c.rgba ());
  ui.pb_targetColorBg->setStyleSheet (QString ("background-color: %1;").arg (targetColorBg.name ()));
}

void
PwmdOptionsDialog::slotChoosePermissionColor ()
{
  QColor c = QColorDialog::getColor (permissionsColor);

  if (!c.isValid ())
    return;

  permissionsColor.setRgba (c.rgba ());
  ui.pb_permissionsColor->setStyleSheet (QString ("background-color: %1;").arg (permissionsColor.name ()));
}

void
PwmdOptionsDialog::slotChoosePermissionColorBg ()
{
  QColor c = QColorDialog::getColor (permissionsColorBg);

  if (!c.isValid ())
    return;

  permissionsColorBg.setRgba (c.rgba ());
  ui.pb_permissionsColorBg->setStyleSheet (QString ("background-color: %1;").arg (permissionsColorBg.name ()));
}

void
PwmdOptionsDialog::slotChooseInvalidTargetColor ()
{
  QColor c = QColorDialog::getColor (invalidTargetColor);

  if (!c.isValid ())
    return;

  invalidTargetColor.setRgba (c.rgba ());
  ui.pb_invalidTargetColor->setStyleSheet (QString ("background-color: %1;").arg (invalidTargetColor.name ()));
}

void
PwmdOptionsDialog::slotChooseInvalidTargetColorBg ()
{
  QColor c = QColorDialog::getColor (invalidTargetColorBg);

  if (!c.isValid ())
    return;

  invalidTargetColorBg.setRgba (c.rgba ());
  ui.pb_invalidTargetColorBg->setStyleSheet (QString ("background-color: %1;").arg (invalidTargetColorBg.name ()));
}

void
PwmdOptionsDialog::slotChooseTargetLoopColor ()
{
  QColor c = QColorDialog::getColor (targetLoopColor);

  if (!c.isValid ())
    return;

  targetLoopColor.setRgba (c.rgba ());
  ui.pb_targetLoopColor->setStyleSheet (QString ("background-color: %1;").arg (targetLoopColor.name ()));
}

void
PwmdOptionsDialog::slotChooseTargetLoopColorBg ()
{
  QColor c = QColorDialog::getColor (targetLoopColorBg);

  if (!c.isValid ())
    return;

  targetLoopColorBg.setRgba (c.rgba ());
  ui.pb_targetLoopColorBg->setStyleSheet (QString ("background-color: %1;").arg (targetLoopColorBg.name ()));
}
