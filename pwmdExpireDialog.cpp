/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QDateTime>
#include <time.h>
#include "pwmdExpireDialog.h"

PwmdExpireDialog::PwmdExpireDialog (time_t e, time_t incr, QWidget *p)
     : QDialog (p)
{
  ui.setupUi (this);
  QDateTime dt;
  dt.setSecsSinceEpoch (e ? e : time (nullptr));
  ui.dt_expire->setDateTime (dt);
  ui.ck_expiry->setChecked (e != 0);

  time_t s = 0, m = 0, h = 0, d = 0, w = 0, mo = 0, n = 0;
  if (incr >= (60*60*24*31))
    {
      mo = incr/(60*60*24*31);
      n = incr%(60*60*24*31);
    }
  else
    n = incr;

  if (n >= (60*60*24*7))
    {
      w = n/(60*60*24*7);
      n = n%(60*60*24*7);
    }

  if (n >= (60*60*24))
    {
      d = n/(60*60*24);
      n = n%(60*60*24);
    }

  if (n >= (60*60))
    {
      h = n/(60*60);
      n = n%(60*60);
    }

  if (n >= 60)
    {
      m = n/60;
      n = n%60;
    }

  s = n;
  ui.sp_second->setValue (s);
  ui.sp_minute->setValue (m);
  ui.sp_hour->setValue (h);
  ui.sp_day->setValue (d);
  ui.sp_week->setValue (w);
  ui.sp_month->setValue (mo);
}

PwmdExpireDialog::~PwmdExpireDialog ()
{
}

time_t
PwmdExpireDialog::expire ()
{
  if (!ui.ck_expiry->isChecked ())
    return 0;

  time_t now = time (nullptr);
  time_t n = ui.dt_expire->dateTime ().toSecsSinceEpoch ();

  return n <= now ? n + increment () : n;
}

time_t
PwmdExpireDialog::increment ()
{
  int s, m, h, d, w, mo;

  s = ui.sp_second->value();
  m = ui.sp_minute->value();
  h = ui.sp_hour->value();
  d = ui.sp_day->value();
  w = ui.sp_week->value();
  mo = ui.sp_month->value();

  mo *= mo ? 60*60*24*31: 1;
  w *= w ? 60*60*24*7 : 1;
  d *= d ? 60*60*24 : 1;
  h *= h ? 60*60 : 1;
  m *= m ? 60 : 1;
  return mo+w+d+h+m+s;
}
