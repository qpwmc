/*
    Copyright (C) 2017-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QPushButton>
#include "pwmdGenPassWidget.h"
#include "pwmdPasswordPromptDialog.h"

PwmdPasswordPromptDialog::PwmdPasswordPromptDialog (QWidget *p) : QDialog (p)
{
  ui.setupUi (this);
  ui.le_passphrase->setInputMethodHints (Qt::ImhNoPredictiveText|Qt::ImhNoAutoUppercase|Qt::ImhSensitiveData);
  connect (ui.le_passphrase, SIGNAL (textChanged (const QString &)), this,
           SLOT (slotPassphraseChanged (const QString &)));
  connect (ui.le_repeat, SIGNAL (textChanged (const QString &)), this,
           SLOT (slotPassphraseChanged (const QString &)));
}

PwmdPasswordPromptDialog::~PwmdPasswordPromptDialog ()
{
}

void
PwmdPasswordPromptDialog::setPrompt (const QString &s)
{
  ui.l_prompt->setText (s);
}

void
PwmdPasswordPromptDialog::setDescription (const QString &s)
{
  ui.l_description->setText (s);
}

QString
PwmdPasswordPromptDialog::passphrase ()
{
  return ui.le_passphrase->text ();
}

void
PwmdPasswordPromptDialog::setRepeat (bool b)
{
  ui.l_repeat->setHidden (!b);
  ui.le_repeat->setHidden (!b);
}

void
PwmdPasswordPromptDialog::setQuality (bool b)
{
  ui.pg_strength->setHidden (!b);
}

void
PwmdPasswordPromptDialog::slotPassphraseChanged (const QString &s)
{
  if (ui.le_repeat->isHidden ())
    return;

  QPushButton *b = ui.buttonBox->button (QDialogButtonBox::Ok);
  b->setEnabled (ui.le_passphrase->text () == ui.le_repeat->text ());

  double bits = 0;
  if (!ui.pg_strength->isHidden ())
    {
      ui.pg_strength->setValue (PwmdGenPassWidget::quality (s,
                                                            PwmdGenPassWidget::MaxEntropyBits,
                                                            bits));
      ui.pg_strength->setFormat (QString (tr ("Strength %p% (%1 bits)")).arg (bits));
    }
}
