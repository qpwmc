/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of qpwmc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <QCommonStyle>
#include "pwmdPasswordDialog.h"
#include "pwmdFileDialog.h"

PwmdPasswordDialog::PwmdPasswordDialog (QWidget *p) : QDialog (p)
{
  QCommonStyle style;
  ui.setupUi (this);
  ui.tb_openKeyFile->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  ui.tb_saveKeyFile->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  ui.tb_signKeyFile->setIcon (QIcon (":icons/rodentia-icons_folder-open-blue.svg"));
  _symmetric = false;
  _newFile = false;
  _signers = false;

  connect (ui.tb_openKeyFile, SIGNAL (clicked()), this, SLOT (slotSelectDecryptKeyFile ()));
  connect (ui.tb_saveKeyFile, SIGNAL (clicked()), this, SLOT (slotSelectEncryptKeyFile ()));
  connect (ui.tb_signKeyFile, SIGNAL (clicked()), this, SLOT (slotSelectSignKeyFile ()));
}

PwmdPasswordDialog::~PwmdPasswordDialog ()
{
}

void
PwmdPasswordDialog::setSymmetric (bool b)
{
  _symmetric = b;
  ui.l_saveKeyFile->setHidden (!b);
  ui.le_saveKeyFile->setHidden (!b);
  ui.tb_saveKeyFile->setHidden (!b);
}

void
PwmdPasswordDialog::setNewFile (bool b)
{
  _newFile = b;
  ui.l_openKeyFile->setHidden (b);
  ui.le_openKeyFile->setHidden (b);
  ui.tb_openKeyFile->setHidden (b);
}

void
PwmdPasswordDialog::setHasSigners (bool b)
{
  _signers = b;
  ui.l_signKeyFile->setHidden (!b);
  ui.le_signKeyFile->setHidden (!b);
  ui.tb_signKeyFile->setHidden (!b);
}

void
PwmdPasswordDialog::slotSelectDecryptKeyFile ()
{
  PwmdFileDialog d (this, tr ("Decryption passphrase file"));

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;
  ui.le_openKeyFile->setText (d.selectedFiles ().at (0));
}

QString
PwmdPasswordDialog::decryptKeyFile ()
{
  return ui.le_openKeyFile->text ();
}

void
PwmdPasswordDialog::setDecryptKeyFile (QString s)
{
  if (!s.isEmpty ())
    ui.gr_keyFile->setChecked (true);

  ui.le_openKeyFile->setText (s);
}

void
PwmdPasswordDialog::slotSelectEncryptKeyFile ()
{
  PwmdFileDialog d (this, tr ("Encryption passphrase file"),
#ifdef Q_OS_ANDROID
                 "/sdcard"
#else
                 QDir::home ().path ()
#endif
  );

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;
  ui.le_saveKeyFile->setText (d.selectedFiles ().at (0));
}

QString
PwmdPasswordDialog::encryptKeyFile ()
{
  return ui.le_saveKeyFile->text ();
}

void
PwmdPasswordDialog::setEncryptKeyFile (QString s)
{
  if (!s.isEmpty ())
    ui.gr_keyFile->setChecked (true);

  ui.le_saveKeyFile->setText (s);
}

void
PwmdPasswordDialog::slotSelectSignKeyFile ()
{
  PwmdFileDialog d (this, tr ("Signing passphrase file"),
#ifdef Q_OS_ANDROID
                 "/sdcard"
#else
                 QDir::home ().path ()
#endif
  );

  d.setFileMode (PwmdFileDialog::ExistingFile);
  d.setOption (PwmdFileDialog::ReadOnly);
#ifdef Q_OS_ANDROID
  d.setWindowState (Qt::WindowFullScreen);
#endif

  if (!d.exec () || !d.selectedFiles ().count ())
    return;
  ui.le_signKeyFile->setText (d.selectedFiles ().at (0));
}

QString
PwmdPasswordDialog::signKeyFile ()
{
  return ui.le_signKeyFile->text ();
}

void
PwmdPasswordDialog::setSignKeyFile (QString s)
{
  if (!s.isEmpty ())
    ui.gr_keyFile->setChecked (true);

  ui.le_signKeyFile->setText (s);
}

bool
PwmdPasswordDialog::useKeyFile ()
{
  return ui.gr_keyFile->isChecked () &&
    (!ui.le_openKeyFile->text().isEmpty ()
    || !ui.le_saveKeyFile->text().isEmpty ()
    || !ui.le_signKeyFile->text().isEmpty ());
}
